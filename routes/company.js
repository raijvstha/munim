const express = require('express')
const router = express.Router()
const bodyParser = require('body-parser')

const auth = require('../middleware/auth')
const { companyPermission } = require('../middleware/permission')

const { runValidation } = require('../validators')
const { createCompanyValidators, editCompanyValidators, userListValidators } = require('../validators/company')
const { ErrorHandler } = require('../helpers/error')
const Company = require('../models/Company')
const User = require('../models/User')



/**
 * @swagger
 *
 * /v1/company/register:
 *   put:
 *     description: Edit company name
 *     consumes:
 *      - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         description: Updated company name
 *         required: true
 *         schema:
 *            properties:
 *              user_id:
 *                type: number
 *                example: "5f7b22a94a0e8633e4349f46"
 *                description: Change above example user id with existing user id 
 *              company_name:
 *                type: string
 *                example: "Company Name"
 *     responses:
 *       200:
 *         description: Company name update successfully
 *       400:
 *         description: Company already exists
 *       404:
 *         description: Company doesnot exists
 */

router.post('/register', [bodyParser.json(), createCompanyValidators, runValidation], async (req, res, next) => {
    const { company_name, user_id } = req.body
    try {
      const user = await User.findOne({ _id: user_id })
      if (!user){
        throw new ErrorHandler(404, 'User doesnot exists')
      } 

      const company = await Company.findOne({company_name})
      if (company){
        throw new ErrorHandler(400, 'Company name exists')
      } 

      const newCompany = await Company.create({ company_name, user_id  })
  
      if (!newCompany) {
        throw new ErrorHandler(400, 'Something went wrong adding company')
      }

      res.status(201).json({
        status: 'ok',
        message: 'Company registered successfully'
      })
    } catch (err) {
      next(err)
    }
});


/**
 * @swagger
 *
 * /v1/company/edit:
 *   put:
 *     description: Edit company name
 *     consumes:
 *      - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         description: Updated company name
 *         required: true
 *         schema:
 *            properties:
 *              user_id:
 *                type: number
 *                example: "5f7b22a94a0e8633e4349f46"
 *                description: Change above example user id with existing user id 
 *              company_id:
 *                type: number
 *                example: "5f7b22a94a0e8633e4349f47"
 *                description: Change above example company id with existing company id
 *              company_name:
 *                type: string
 *                example: "test company"
 *     responses:
 *       200:
 *         description: Company name update successfully
 *       400:
 *         description: Company already exists
 *       404:
 *         description: Company doesnot exists
 */
router.put('/edit', [bodyParser.json(), editCompanyValidators, runValidation, companyPermission], async (req, res, next) => {
  const { company_name, company_id } = req.body
  //res.send(req.user)
  try {
    const companyExists = await Company.findOne({ company_name })
    if (companyExists){
      throw new ErrorHandler(400, 'Company already exists')
    } 
    const company = await Company.findOne({ _id: company_id })
    if(!company){
      throw new ErrorHandler(404, 'Company doesnot exists')
    }
    company.company_name = company_name ? company_name : company.company_name
    const updatedCompany  =  await company.save()
    if(!updatedCompany){
      throw new ErrorHandler(400, 'Something went wrong updating company')
    }
    
    res.json({
      status: 'ok',
      message: 'Company name updated successfully'
    })
  } catch (err) {
    next(err)
  }
});


/**
 * @swagger
 *
 * /v1/company/list:
 *   post:
 *     description: List companies according to user_id
 *     consumes:
 *      - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         description: User Id
 *         required: true
 *         schema:
 *            properties:
 *              user_id:
 *                type: number
 *                example: "5f7b22a94a0e8633e4349f46"
 *                description: Change above example user id with existing user id 
 *              
 *     responses:
 *       200:
 *         description: Company List
 *       404:
 *         description: User doesnot exists
 */
router.post('/list', [bodyParser.json(), userListValidators, runValidation], async (req, res, next) => {
  const { user_id } = req.body
  try {
    const user = await User.findOne({ _id: user_id })
    if (!user){
      throw new ErrorHandler(404, 'User doesnot exists')
    } 
    const companies = await Company.find({ user_id }).select("company_name user_id")
    let result = []
    if(companies && companies.length > 0){
      for(company of companies){
        result.push({
          user_id: company.user_id,
          company_id: company._id,
          company_name: company.company_name
        })
      }
    }
    res.json({
      status: 'ok',
      message: 'Company List',
      result
    })
  } catch (err) {
    next(err)
  }
});



module.exports = router
