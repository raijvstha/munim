const express = require('express')
const router = express.Router()
const bodyParser = require('body-parser')

const auth = require('../middleware/auth')

const { runValidation } = require('../validators')
const { addLoanValidators, editLoanValidators, payBackLoanValidators, getstaffLoansValidators, deletestaffLoansValidators } = require('../validators/loan')
const { staffPermission } = require('../middleware/permission')
const { ErrorHandler } = require('../helpers/error')
const Loan = require('../models/Loan')


/**
 * @swagger
 *
 * /v1/loan/add:
 *   post:
 *     description: Add loan to staff
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         description: Detail object of loan to be added
 *         required: true
 *         schema:
 *            properties:
 *              staff_id:
 *                type: number
 *                example: "5f7c1e456a5d5041d77acf11"
 *                description: Change above example staff id with existing staff id 
 *              company_id:
 *                type: number
 *                example: "5f7b22a94a0e8633e4349f47"
 *                description: Change above example company id with existing company id
 *              amount: 
 *                type: number
 *                example: "5000"
 *              date:
 *                type: date
 *                example: "2020-03-30"
 *                description: Date must be on YYYY-MM-DD format
 *              description:
 *                type: string
 *                example: "Loan given"
 * 
 *     responses:
 *       201:
 *         description: Loan added successfully
 *       400:
 *         description: Something went wrong on issuing loan
 */

router.post('/add', [bodyParser.json(), addLoanValidators, runValidation, staffPermission], async (req, res, next) => {
  const {staff_id, amount, date, description} = req.body
  const status = "unpaid"
  try {
    const loan = await Loan.create({ staff_id, amount, description, issue_date: date, status })
    if (!loan) {
      throw new ErrorHandler(400, 'Something went wrong on issuing loan')
    }
    res.status(201).json({
      status: 'ok',
      message: 'Loan added successfully'
    })
  } catch (err) {
    next(err)
  }
});


/**
 * @swagger
 *
 * /v1/loan/edit:
 *   put:
 *     description: Edit loan added to staff
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         description:  Detail object of loan to be edited
 *         required: true
 *         schema:
 *            properties:
 *              company_id:
 *                type: number
 *                example: "5f7b22a94a0e8633e4349f47"
 *                description: Change above example company id with existing company id
 *              staff_id:
 *                type: number
 *                example: "5f7c1e456a5d5041d77acf11"
 *                description: Change above example staff id with existing staff id 
 *              loan_id:
 *                type: number
 *                example: "5f7d471f519fb887a637b495"
 *                description: Change above example loan id with existing loan id
 *              amount: 
 *                type: number
 *                example: "5000"
 *              date:
 *                type: date
 *                example: "2020-03-30"
 *                description: Date must be on YYYY-MM-DD format
 *              description:
 *                type: string
 *                example: "Edit loan given"
 * 
 *     responses:
 *       200:
 *         description: Loan updated successfully
 *       404:
 *         description: No any loan is issued
 */

router.put('/edit', [bodyParser.json(), editLoanValidators, runValidation, staffPermission], async (req, res, next) => {
  const {loan_id, staff_id, amount, date, description} = req.body 
  try {
    const loan = await Loan.findOne({ _id: loan_id, staff_id })
    if(!loan){
      throw new ErrorHandler(404, 'No any loan is issued')
    }
    loan.amount = amount ? amount : loan.amount
    loan.description = description ? description : loan.description
    loan.issue_date = date ? date : loan.issue_date
    const updatedLoan  =  await loan.save()
    if(!updatedLoan) throw Error ('Something went wrong updating Loan')
    res.json({
      status: 'ok',
      message: 'Loan updated successfully',
      updatedLoan
    })
  } catch (err) {
      next(err)
  }
});


/**
 * @swagger
 *
 * /v1/loan/payback:
 *   put:
 *     description: Set payback amount of loan form when it should deduct
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         description:  Payback object details
 *         required: true
 *         schema:
 *            properties:
 *              company_id:
 *                type: number
 *                example: "5f7b22a94a0e8633e4349f47"
 *                description: Change above example company id with existing company id
 *              staff_id:
 *                type: number
 *                example: "5f7c1e456a5d5041d77acf11"
 *                description: Change above example staff id with existing staff id 
 *              loan_id:
 *                type: number
 *                example: "5f7d471f519fb887a637b495"
 *                description: Change above example loan id with existing loan id
 *              amount: 
 *                type: number
 *                example: "5000"
 *              date:
 *                type: date
 *                example: "2020-05-01"
 *                description: Date must be on YYYY-MM-DD format
 *              pay_back_every_month:
 *                type: string
 *                example: "true"
 *                enum:
 *                  - true
 *                  - false
 *              description:
 *                type: string
 *                example: "Set payback to loan given"
 * 
 *     responses:
 *       200:
 *         description: Loan updated successfully
 *       404:
 *         description: No any loan is issued
 */

router.put('/payback', [bodyParser.json(), payBackLoanValidators, runValidation, staffPermission], async (req, res, next) => {
  const {loan_id, staff_id, amount, date, description, pay_back_every_month} = req.body
  try {
    const loan = await Loan.findOne({ _id: loan_id, staff_id })
    if(!loan){
      throw new ErrorHandler(404, 'No any loan is issued')
    }
    if(loan.payback_date){
      throw new ErrorHandler(400, 'Payback is already set')
    }
    const set_pay_back_every_month = pay_back_every_month ? pay_back_every_month : "false"
    loan.payback_amount = amount
    loan.payback_description = description
    loan.payback_date = date
    loan.pay_back_every_month = set_pay_back_every_month
    const payback  =  await loan.save()
    if(!payback) throw Error ('Something went wrong updating Loan')
    res.json({
      status: 'ok',
      message: 'Loan payback is set successfully'
    })
  } catch (err) {
    next(err)
  }
});


/**
 * @swagger
 *
 * /v1/loan/staff-loan-list:
 *   post:
 *     description: List of Loan taken by staff
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         description:  Staff Id
 *         required: true
 *         schema:
 *            properties:
 *              company_id:
 *                type: number
 *                example: "5f7b22a94a0e8633e4349f47"
 *                description: Change above example company id with existing company id
 *              staff_id:
 *                type: number
 *                example: "5f7c1e456a5d5041d77acf11"
 *                description: Change above example staff id with existing staff id 
 * 
 *     responses:
 *       200:
 *         description: Lists of loan taken
 *       404:
 *         description: No any loan is issued
 */

router.post('/staff-loan-list', [bodyParser.json(), getstaffLoansValidators, runValidation, staffPermission], async (req, res, next) => {
  const { staff_id } = req.body
  try {
    const loans = await Loan.find({ staff_id, "status": "unpaid" })
    if(!loans){
      throw new ErrorHandler(404, 'No any loan is issued')
    }
    res.json({
      status: 'ok',
      message: 'Lists of loan taken',
      loans
    })
  } catch (err) {
    next(err)
  }
});


/**
 * @swagger
 *
 * /v1/loan/staff-loan-cleared-list:
 *   post:
 *     description: List of Loan taken by staff
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         description:  Staff Id
 *         required: true
 *         schema:
 *            properties:
 *              company_id:
 *                type: number
 *                example: "5f7b22a94a0e8633e4349f47"
 *                description: Change above example company id with existing company id
 *              staff_id:
 *                type: number
 *                example: "5f7c1e456a5d5041d77acf11"
 *                description: Change above example staff id with existing staff id 
 * 
 *     responses:
 *       200:
 *         description: Lists of loan cleared
 *       404:
 *         description: No any loan is issued
 */

router.post('/staff-loan-cleared-list', [bodyParser.json(), getstaffLoansValidators, runValidation, staffPermission], async (req, res, next) => {
  const { staff_id } = req.body
  try {
    const loans = await Loan.find({ staff_id, "status": "paid" })
    if(!loans){
      throw new ErrorHandler(404, 'No any loan is issued')
    }
    res.json({
      status: 'ok',
      message: 'Lists of loan cleared',
      loans
    })
  } catch (err) {
    next(err)
  }
});

/**
 * @swagger
 *
 * /v1/loan/staff-loan-amount:
 *   post:
 *     description: Loan amount given to staff
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         description:  Staff Id
 *         required: true
 *         schema:
 *            properties:
 *              company_id:
 *                type: number
 *                example: "5f7b22a94a0e8633e4349f47"
 *                description: Change above example company id with existing company id
 *              staff_id:
 *                type: number
 *                example: "5f7c1e456a5d5041d77acf11"
 *                description: Change above example staff id with existing staff id 
 * 
 *     responses:
 *       200:
 *         description: Loan given to staff
 *       404:
 *         description: No any loan is issued
 */

router.post('/staff-loan-amount', [bodyParser.json(), getstaffLoansValidators, runValidation, staffPermission], async (req, res, next) => {
  const { staff_id } = req.body
  try {
    const loans = await Loan.find({ staff_id }).select("amount")
    let loanAmount = 0
    if(!loans){
      throw new ErrorHandler(404, 'No any loan is issued')
    }
    loans.forEach(loan => {
      loanAmount = loanAmount + loan.amount
    });
    res.json({
      status: 'ok',
      message: 'Loan given to staff',
      amount: loanAmount
    })
  } catch (err) {
    next(err)
  }
});


/**
 * @swagger
 *
 * /v1/loan/staff-loan-cleared-amount:
 *   post:
 *     description: Loan amount cleared by staff
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         description:  Staff Id
 *         required: true
 *         schema:
 *            properties:
 *              company_id:
 *                type: number
 *                example: "5f7b22a94a0e8633e4349f47"
 *                description: Change above example company id with existing company id
 *              staff_id:
 *                type: number
 *                example: "5f7c1e456a5d5041d77acf11"
 *                description: Change above example staff id with existing staff id 
 * 
 *     responses:
 *       200:
 *         description: Loan cleared by staff
 *       404:
 *         description: No any loan is issued
 */

router.post('/staff-loan-cleared-amount', [bodyParser.json(), getstaffLoansValidators, runValidation, staffPermission], async (req, res, next) => {
  const { staff_id } = req.body
  try {
    const loans = await Loan.find({ staff_id, status: "paid" }).select("amount")
    let loanPaidAmount = 0
    if(!loans){
      throw new ErrorHandler(404, 'No any loan is issued')
    }
    loans.forEach(loan => {
      loanPaidAmount = loanPaidAmount + loan.amount
    });
    res.json({
      status: 'ok',
      message: 'Loan cleared by staff',
      amount: loanPaidAmount
    })
  } catch (err) {
    next(err)
  }
});

/**
 * @swagger
 *
 * /v1/loan/delete:
 *   post:
 *     description: Delete loan of staff
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         description: Detail object of loan to be deleted
 *         required: true
 *         schema:
 *            properties:
 *              loan_id:
 *                type: number
 *                example: "5f9519dbdf04fd6bbd7af8da"
 *                description: Change above example loan id with existing loan id 
 *              staff_id:
 *                type: number
 *                example: "5f7c1e456a5d5041d77acf11"
 *                description: Change above example staff id with existing staff id 
 *              company_id:
 *                type: number
 *                example: "5f7b22a94a0e8633e4349f47"
 *                description: Change above example company id with existing company id
 * 
 *     responses:
 *       200:
 *         description: Loan deleted successfully
 *       404:
 *         description: Loan does not exists
 *       400:
 *         description: Unable to deleted loan
 */

router.post('/delete', [bodyParser.json(), deletestaffLoansValidators, runValidation, staffPermission], async (req, res, next) => {
  const { loan_id } = req.body
  try {
    const loan = await Loan.findOne({ _id: loan_id })
    if (!loan){
      throw new ErrorHandler(404, 'Loan does not exists')
    } 
    const newLoan = await Loan.findOneAndRemove({ _id: loan_id  })
    const deleteLoan = await Loan.findOne({ _id: newLoan._id })
    
    if (!deleteLoan) {
      res.json({
        status: 'ok',
        message: 'Loan deleted Successfully'
      })
    }else{
      res.statusCode(400).json({
        message: 'Unable to deleted loan'
      })
    }
  } catch (err) {
    next(err)
  }
});

module.exports = router