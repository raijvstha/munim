const express = require('express')
const router = express.Router()
const bodyParser = require('body-parser')
const moment = require('moment')
const auth = require('../middleware/auth')

const { runValidation } = require('../validators')
const { addBonusAllowanceValidators, allBonusAllowanceValidators } = require('../validators/bonusallowance')
const { staffPermission } = require('../middleware/permission')
const { checkDate, checkStatement } = require('../middleware/check')
const { ErrorHandler } = require('../helpers/error')
const BonusAllowance = require('../models/BonusAllowance')
const SalaryStatement = require('../models/SalaryStatement')

/**
 * @swagger
 *
 * /v1/bonus-allowance/add:
 *   post:
 *     description: Add Bounus or Allowance to staff
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         description: Detail object of Bonus or Allowance to be added
 *         required: true
 *         schema:
 *            properties:
 *              staff_id:
 *                type: number
 *                example: "5f7c1e456a5d5041d77acf11"
 *                description: Change above example staff id with existing staff id 
 *              company_id:
 *                type: number
 *                example: "5f7b22a94a0e8633e4349f47"
 *                description: Change above example company id with existing company id
 *              type:
 *                type: string
 *                example: "allowance"
 *                enum:
 *                  - allowance
 *                  - bonus
 *              amount: 
 *                type: number
 *                example: "5000"
 *              date:
 *                type: date
 *                example: "2020-03-30"
 *                description: Date must be on YYYY-MM-DD format
 *              description:
 *                type: string
 *                example: "Loan given"
 * 
 *     responses:
 *       201:
 *         description: Bonus or Allowance added successfully
 *       400:
 *         description: Something went wrong adding Bonus or Allowance 
 */

router.post('/add', [bodyParser.json(), addBonusAllowanceValidators, runValidation, staffPermission, checkDate, checkStatement], async (req, res, next) => {
    const { staff_id, amount, date, description, type } = req.body
    const startMonth = new moment(date).startOf('month').format('YYYY-MM-DD[T00:00:00.000Z]')
    const endMonth = new moment(date).endOf("month").format('YYYY-MM-DD[T00:00:00.000Z]')
    try{
      const currentStatement = await SalaryStatement.findOne({ staff_id, date: { "$gte": startMonth, "$lte": endMonth}})
      if(!currentStatement){
        throw new ErrorHandler(400, "No statement found to add Bonus or Allowances")
      }
      const newBonusAllowance = await BonusAllowance.create({ staff_id, amount, date, description, type })
      if (!newBonusAllowance) {
          throw new ErrorHandler(400, `Something went wrong adding ${type}`)
      }
      // Updating statement
      currentStatement.amount = (parseInt(currentStatement.amount) + parseInt(amount)).toFixed(2)
      const updateStatement = await currentStatement.save()
      res.status(201).json({
        status: 'ok',
        message: `${type} added successfully`
      })
    } catch(err){
      next(err)
    }
});


/**
 * @swagger
 *
 * /v1/bonus-allowance/all-bonus-allowance:
 *   post:
 *     description: List all bounus and allowance of staff
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         description: Detail object of Bonus or Allowance to be added
 *         required: true
 *         schema:
 *            properties:
 *              staff_id:
 *                type: number
 *                example: "5f7c1e456a5d5041d77acf11"
 *                description: Change above example staff id with existing staff id 
 *              company_id:
 *                type: number
 *                example: "5f7b22a94a0e8633e4349f47"
 *                description: Change above example company id with existing company id
 * 
 *     responses:
 *       200:
 *         description: Array of bonus and allowances
 *       400:
 *         description: No any bonus and allowances given
 */

router.post('/all-bonus-allowance', [bodyParser.json(), allBonusAllowanceValidators, runValidation, staffPermission], async (req, res, next) => {
  const { staff_id } = req.body
  try{
    const result = await BonusAllowance.find({ staff_id },  { _id: 0, 'staff_id':0, 'createdAt': 0, "updatedAt": 0, '__v': 0 })
    if(!result){
      throw new ErrorHandler(404, `No any bonus and allowances given`)
    }
  
    res.json({
      status: 'ok',
      result
    })
  } catch(err){
    next(err)
  }
});


module.exports = router