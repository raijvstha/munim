const express = require('express')
const router = express.Router()
const bodyParser = require('body-parser')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
 

const auth = require('../middleware/auth')

const { runValidation } = require('../validators')
const { userSignupValidators, userLoginValidators, searchbyphoneValidators, userUpdateValidators } = require('../validators/auth')
const { ErrorHandler } = require('../helpers/error')

const Company = require('../models/Company')
const User = require('../models/User')

/**
 * @swagger
 *
 * /v1/signup:
 *   post:
 *     description: Signup to the application
 *     consumes:
 *      - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         description: Detail object of signup company
 *         required: true
 *         schema:
 *            properties:
 *              company_name:
 *                type: string
 *                example: "test company"
 *              full_name:
 *                type: string
 *                example: "test name"  
 *              phone:
 *                type: number
 *                example: "9804050555"
 *     responses:
 *       201:
 *         description: Lists details of user
 *       404:
 *         description: User Does not exist
 */

router.post('/signup', [bodyParser.json(), userSignupValidators, runValidation], async (req, res, next) => {
  const { company_name, full_name, email, phone } = req.body
  try {
    const user = await User.findOne({ phone })
    if (user){
      throw new ErrorHandler(400, 'Phone already exists')
    } 
    const company = await Company.findOne({ company_name })
    if (company){
      throw new ErrorHandler(400, 'Company already exists')
    } 
    console.log(email)
    if(email){
      const userEmail = await User.findOne({ email })
      if (userEmail){
        throw new ErrorHandler(400, 'Email already exists')
      } 
    }
   
    const newUser = await User.create({  phone, full_name, email,  role: 'owner'})
    const newCompany = await Company.create({ company_name, user_id: newUser._id  })
   
    if (!newUser && !newCompany) {
      throw new ErrorHandler(400, 'Something went wrong creating user')
    }

    res.status(201).json({
      status: 'ok',
      message: 'Company registered successfully'
    })

  } catch (err) {
    next(err)
  }
});

/**
 * @swagger
 *
 * /v1/signin:
 *   post:
 *     description: Login to the application
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         description: Phone number to be signin
 *         required: true
 *         schema:
 *            properties:
 *              phone:
 *                type: number
 *                example: "9804050555"
 *     responses:
 *       200:
 *         description: Lists details of user
 *       404:
 *         description: User Does not exist
 */

router.post('/signin', [bodyParser.json(), userLoginValidators, runValidation], async (req, res, next) => {
  const { phone } = req.body
  try {
    const user = await User.findOne({ phone })
    if (!user){
      throw new ErrorHandler(404, 'User Does not exist');
    } 
    const company = await Company.findOne({ user_id: user._id })
    if(!company){
      throw new ErrorHandler(404, 'Company Does not exist');
    }

    const data = {
      user_id: user._id,
      company_id: company._id,
      company_name: company.company_name,
      full_name: user.full_name,
      phone: user.phone,
      email: user.email ? user.email : ""
    }

    // const isMatch = bcrypt.compareSync(password, user.password)
    // if (!isMatch){
    //   throw new ErrorHandler(400, 'Invalid credentials');
    // } 
    // const token = jwt.sign(
    //   { 
    //     userid: user._id,
    //     username: user.username,  
    //     email: user.email,
    //     fullname: user.full_name,
    //     role: user.role // TODO: later set role and access
    //   },
    //   process.env.JWT_SECRET, 
    //   { expiresIn: '1d' }
    // );
    // if (!token){
    //   throw new ErrorHandler(400, 'Couldnt sign the token');
    // } 
   // res.cookie('token', token, { expiresIn: '1d' })

    res.json({
      status: 'ok',
      data
    });
  } catch (err) {
    next(err)
  }
});


/**
 * @swagger
 *
 * /v1/searchbyphone:
 *   post:
 *     description: Search user by phone number to the application
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         description: Phone number to be searched
 *         required: true
 *         schema:
 *            properties:
 *              phone:
 *                type: number
 *                example: "9804050555"
 *     responses:
 *       200:
 *         description: User doesnot exist with this phone number
 *       400:
 *         description: User exists with this phone number
 */

router.post('/searchbyphone', [bodyParser.json(), searchbyphoneValidators, runValidation], async (req, res, next) => {
  const { phone } = req.body
  try {
    const user = await User.findOne({ phone })
    if (user){
      throw new ErrorHandler(400, 'User exists with this phone number');
    } 
    res.json({
      status: 'ok',
      message: 'User doesnot exist with this phone number',
      code: true
    });
  } catch (err) {
    next(err)
  }
});



router.post('/admin-signin', [bodyParser.json(), userLoginValidators, runValidation], async (req, res, next) => {
  const { phone } = req.body
  try {
    const user = await User.findOne({ phone })
    if (!user){
      throw new ErrorHandler(404, 'User Does not exist');
    } 
    const company = await Company.findOne({ user_id: user._id })
    if(!company){
      throw new ErrorHandler(404, 'Company Does not exist');
    }

    const data = {
      user_id: user._id,
      company_id: company._id,
      company_name: company.company_name,
      full_name: user.full_name,
      phone: user.phone,
      email: user.email ? user.email : ""
    }

    // const isMatch = bcrypt.compareSync(password, user.password)
    // if (!isMatch){
    //   throw new ErrorHandler(400, 'Invalid credentials');
    // } 
    // const token = jwt.sign(
    //   { 
    //     userid: user._id,
    //     username: user.username,  
    //     email: user.email,
    //     fullname: user.full_name,
    //     role: user.role // TODO: later set role and access
    //   },
    //   process.env.JWT_SECRET, 
    //   { expiresIn: '1d' }
    // );
    // if (!token){
    //   throw new ErrorHandler(400, 'Couldnt sign the token');
    // } 
   // res.cookie('token', token, { expiresIn: '1d' })

    res.json({
      status: 'ok',
      data
    });
  } catch (err) {
    next(err)
  }
});


router.post('/admin-signup', [bodyParser.json(), userSignupValidators, runValidation], async (req, res, next) => {
  const { company_name, full_name, email, phone } = req.body
  try {
    const user = await User.findOne({ phone })
    if (user){
      throw new ErrorHandler(400, 'Phone already exists')
    } 
    const company = await Company.findOne({ company_name })
    if (company){
      throw new ErrorHandler(400, 'Company already exists')
    } 
   
    const newUser = await User.create({  phone, full_name, email,  role: 'owner'})
    const newCompany = await Company.create({ company_name, user_id: newUser._id  })
   
    if (!newUser && !newCompany) {
      throw new ErrorHandler(400, 'Something went wrong creating user')
    }

    res.status(201).json({
      status: 'ok',
      message: 'Company registered successfully'
    })

  } catch (err) {
    next(err)
  }
});



/**
 * @swagger
 *
 * /v1/user/edit:
 *   put:
 *     description: Update user details
 *     consumes:
 *      - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         description: Detail object of user data to be edited
 *         required: true
 *         schema:
 *            properties:
 *              user_id:
 *                type: number
 *                example: "5f7b22a94a0e8633e4349f46"
 *                description: Change above example user id with existing user id 
 *              full_name:
 *                type: string
 *                example: "test name"  
 *              phone:
 *                type: number
 *                example: "9804050555"
 *     responses:
 *       200:
 *         description: User updated successfully
 *       404:
 *         description: User does not exists
 */

router.put('/user/edit', [bodyParser.json(), userUpdateValidators, runValidation], async (req, res, next) => {
  const { user_id, full_name } = req.body
  try {
    const user = await User.findOne({ _id: user_id })
    if (!user){
      throw new ErrorHandler(404, 'User doesnot exists')
    } 
    const userPhone = await User.findOne({ phone })
    if (userPhone){
      throw new ErrorHandler(400, 'Phone already exists')
    } 
 
    user.full_name = full_name ? full_name : user.full_name
  
    const updatedUser  =  await user.save()
   
    if (!updatedUser) {
      throw new ErrorHandler(400, 'Something went wrong updating user')
    }

    res.json({
      status: 'ok',
      message: 'User updated successfully',
      user: {
        _id: updatedUser._id,
        full_name: updatedUser.full_name,
        phone: updatedUser.phone,
        email: updatedUser.email
      }
    })

  } catch (err) {
    next(err)
  }
});

module.exports = router