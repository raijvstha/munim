const express = require('express')
const router = express.Router()
const bodyParser = require('body-parser')

const auth = require('../middleware/auth')

const { runValidation } = require('../validators')
const { calculateSalaryValidators, paySalaryValidators, salaryListsValidators } = require('../validators/payment')
const { staffPermission } = require('../middleware/permission')
const { checkDate, checkStatement } = require('../middleware/check')
const { ErrorHandler } = require('../helpers/error')
const BonusAllowance = require('../models/BonusAllowance')
const Loan = require('../models/Loan')
const Latefine = require('../models/Latefine')
const Attendance = require('../models/Attendance')
const Staff = require('../models/Staff')
const Overtime = require('../models/Overtime')
const Deduction = require('../models/Deduction')
const moment = require('moment')
const Payment = require('../models/Payment')
const SalaryStatement = require('../models/SalaryStatement')

router.post('/calculate', [bodyParser.json(), calculateSalaryValidators, runValidation, staffPermission], async (req, res, next) => {
  const { staff_id } = req.body
  const startMonth = new moment().startOf('month').format('YYYY-MM-DD[T00:00:00.000Z]')
  const endMonth = new moment().endOf("month").format('YYYY-MM-DD[T00:00:00.000Z]')
  
  try{
    var latefineAmount = 0, overtimeAmount = 0, allowanceAmount = 0, bonusAmount = 0, salary = 0, payBackAmount = 0, decuctionAmount = 0, absentAmount = 0,
    halfDayAmount = 0, paidHolidayAmount = 0
    const staff = await Staff.findOne({ _id: staff_id })
    const staffEmploymentType = staff.employment_type
    const hourlyRate = staff.hourly_salary
    const workingHours = staff.working_hours
    const dailySalary = (hourlyRate * workingHours).toFixed(2)


    if(staffEmploymentType === "monthly"){
      salary = staff.monthly_salary
    }

    // Attendence
    const present = await Attendance.find({ date: { "$gte": startMonth, "$lte": endMonth}, staff_id, present: true }).countDocuments()
    const absent = await Attendance.find({ date: { "$gte": startMonth, "$lte": endMonth}, staff_id, absent: true }).countDocuments()
    const halfday = await Attendance.find({ date: { "$gte": startMonth, "$lte": endMonth}, staff_id, halfday: true }).countDocuments()
    const paidholiday = await Attendance.find({ date: { "$gte": startMonth, "$lte": endMonth}, staff_id, paidholiday: true }).countDocuments()

    if(absent > 0){
      absentAmount = (absent * dailySalary).toFixed(2)
    }
    if(halfday > 0){
      halfDayAmount = ((halfday * dailySalary)/2).toFixed(2)
    }
    if(paidholiday > 0){
      paidHolidayAmount = (paidHolidayAmount * dailySalary).toFixed(2)
    }

    // Overtime
    const overtime = await Overtime.find({ date: { "$gte": startMonth, "$lte": endMonth}, staff_id }) 
    if(overtime && overtime.length > 0){
      for (OT of overtime){
        overtimeAmount = overtimeAmount + OT.amount
      }
    }

    // Allowances
    const allowance = await BonusAllowance.find({ date: { "$gte": startMonth, "$lte": endMonth}, staff_id, type: "allowance" })
    const bonus = await BonusAllowance.find({ date: { "$gte": startMonth, "$lte": endMonth}, staff_id, type: "bonus" })
    if(allowance && allowance.length > 0){
      for (AL of allowance){
        allowanceAmount = allowanceAmount + AL.amount
      }
    }
    if(bonus && bonus.length > 0){
      for (BNS of bonus){
        bonusAmount = bonusAmount + BNS.amount
      }
    }
   
    // Latefine
    const latefine = await Latefine.find({ date: { "$gte": startMonth, "$lte": endMonth}, staff_id }) 
    if(latefine && latefine.length > 0){
      for (LF of latefine){
        latefineAmount = latefineAmount + LF.amount
      }
    }

    // Deductions
    const deduction = await Deduction.find({ staff_id, date: { "$lte": endMonth } }) 
    if(deduction && deduction.length > 0){
      for (DEDUCT of deduction){
        decuctionAmount = decuctionAmount + DEDUCT.amount 
      }
    }

    // Loan
    const loan = await Loan.find({ staff_id, status: "unpaid", payback_date: { "$lte": endMonth } }) 
    if(loan && loan.length > 0){
      for (LON of loan){
        if(LON.amount > LON.payback_amount  ){
          payBackAmount = payBackAmount + LON.payback_amount 
        } else {
          payBackAmount = payBackAmount + LON.amount 
        }
      }
    }

    // Calculation
    if(staffEmploymentType === "monthly"){
      //salary = (parseInt(salary) + parseInt(bonusAmount) + parseInt(allowanceAmount) + parseInt(overtimeAmount) ) - parseInt(latefineAmount) 
      salary = ((salary + bonusAmount + allowanceAmount + overtimeAmount + paidHolidayAmount) - latefineAmount - payBackAmount - decuctionAmount - absentAmount - halfDayAmount).toFixed(2)
    }

    if(staffEmploymentType === "hourly"){
      //salary = (parseInt(salary) + parseInt(bonusAmount) + parseInt(allowanceAmount) + parseInt(overtimeAmount) ) - parseInt(latefineAmount) 
      salary = 0000
    }

    res.json({
      status: 'ok',
      attendance: {
        overtimeAmount,
        latefineAmount,
        allowanceAmount,
        bonusAmount,
        dailySalary,
        absent,
        absentAmount,
        halfDayAmount,
        payBackAmount,
        loan,
        payBackAmount,
        decuctionAmount,
        salary
      }
    })
    // console.log(present)
    // console.log(absent)
    // console.log(halfday)
    // console.log(paidholiday)

    // console.log(allowance)
    // console.log(bonus)
    // console.log(latefine)
    // console.log(overtime)

    // res.json(staffEmploymentType)
    
    // const attendance = await Attendance.find({ 
    //   staff_id,
    //   date: { 
    //     "$gte": startMonth, 
    //     "$lte": endMonth
    //   }  
    // })
     
    //console.log(result)
   
  } catch (err) {
    next(err)
  }
});


/**
 * @swagger
 *
 * /v1/payment/add:
 *   post:
 *     description: Pay amount to staff
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         description: Detail object of loan to be added
 *         required: true
 *         schema:
 *            properties:
 *              staff_id:
 *                type: number
 *                example: "5f7c1e456a5d5041d77acf11"
 *                description: Change above example staff id with existing staff id 
 *              company_id:
 *                type: number
 *                example: "5f7b22a94a0e8633e4349f47"
 *                description: Change above example company id with existing company id
 *              amount: 
 *                type: number
 *                example: "5000"
 *              date:
 *                type: date
 *                example: "2020-03-30"
 *                description: Date must be on YYYY-MM-DD format
 *              description:
 *                type: string
 *                example: "Loan given"
 * 
 *     responses:
 *       200:
 *         description: Paid successfully
 *       400:
 *         description: Something went wrong on paying salary
 */

router.post('/add', [bodyParser.json(), paySalaryValidators, runValidation, staffPermission, checkDate, checkStatement], async (req, res, next) => {
  const { staff_id, amount, date, description} = req.body
  const startMonth = new moment(date).startOf('month').format('YYYY-MM-DD[T00:00:00.000Z]')
  const endMonth = new moment(date).endOf("month").format('YYYY-MM-DD[T00:00:00.000Z]')
  try{
    const currentStatement = await SalaryStatement.findOne({ staff_id, date: { "$gte": startMonth, "$lte": endMonth}})
    if(!currentStatement){
      throw new ErrorHandler(400, "No statement found to Payment")
    }
    const salaryPaid = await Payment.create({ staff_id, amount, date, description })
    if (!salaryPaid) {
      throw new ErrorHandler(400, 'Something went wrong on paying salary')
    }

    // const loan = await Loan.find({ staff_id, status: "unpaid", payback_date: { "$lte": endMonth } }) 
    // console.log(loan)
    
    // Updating statement
    currentStatement.amount = (parseInt(currentStatement.amount) - parseInt(amount)).toFixed(2)
    const updateStatement = await currentStatement.save()
    res.json({
      status: 'ok',
      message: 'Paid successfully'
    })
  }catch(err){
    next(err)
  }
})


/**
 * @swagger
 *
 * /v1/payment/all:
 *   post:
 *     description: List all payments according to month
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         description: Detail object of loan to be added
 *         required: true
 *         schema:
 *            properties:
 *              staff_id:
 *                type: number
 *                example: "5f7c1e456a5d5041d77acf11"
 *                description: Change above example staff id with existing staff id 
 *              company_id:
 *                type: number
 *                example: "5f7b22a94a0e8633e4349f47"
 *                description: Change above example company id with existing company id
 *              date:
 *                type: date
 *                example: "2020-03-30"
 *                description: Date must be on YYYY-MM-DD format
 * 
 *     responses:
 *       200:
 *         description: Salary Lists
 *       400:
 *         description: Payment not done
 */

router.post('/all', [bodyParser.json(), salaryListsValidators, runValidation, staffPermission], async (req, res, next) => {
  const { staff_id, date} = req.body
  const startMonth = new moment(date).startOf('month').format('YYYY-MM-DD[T00:00:00.000Z]')
  const endMonth = new moment(date).endOf("month").format('YYYY-MM-DD[T00:00:00.000Z]')
  try{
    const salary = await Payment.find({ staff_id, date: { "$gte": startMonth, "$lte": endMonth}}).select("date amount description")
    if (!salary) {
      throw new ErrorHandler(404, 'Payment not done')
    }
    res.json({
      status: 'ok',
      message: 'Salary Lists',
      salary
    })
  }catch(err){
    next(err)
  }
})

module.exports = router