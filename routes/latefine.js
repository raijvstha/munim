const express = require('express')
const router = express.Router()
const bodyParser = require('body-parser')
const moment = require('moment')
const auth = require('../middleware/auth')

const { runValidation } = require('../validators')
const { addLatefineValidators } = require('../validators/latefine')
const { staffPermission } = require('../middleware/permission')
const { checkDate, checkStatement } = require('../middleware/check')
const { ErrorHandler } = require('../helpers/error')
const Staff = require('../models/Staff')
const Latefine = require('../models/Latefine')
const SalaryStatement = require('../models/SalaryStatement')


/**
 * @swagger
 *
 * /v1/latefine/add:
 *   post:
 *     description: Add Latefine to staff
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         description: Detail object of Latefine to be added
 *         required: true
 *         schema:
 *            properties:
 *              staff_id:
 *                type: number
 *                example: "5f7c1e456a5d5041d77acf11"
 *                description: Change above example staff id with existing staff id 
 *              company_id:
 *                type: number
 *                example: "5f7b22a94a0e8633e4349f47"
 *                description: Change above example company id with existing company id        
 *              duration:
 *                type: number
 *                example: "160"
 *                description: Duration must be in minutes
 * 
 *     responses:
 *       201:
 *         description: Latefine added successfully
 *       400:
 *         description: Latefine exists for today
 */

router.post('/add', [bodyParser.json(), addLatefineValidators, runValidation, staffPermission, checkDate, checkStatement], async (req, res, next) => {
    const { staff_id, company_id, duration } = req.body
    const startMonth = new moment().startOf('month').format('YYYY-MM-DD[T00:00:00.000Z]')
    const endMonth = new moment().endOf("month").format('YYYY-MM-DD[T00:00:00.000Z]')
    try {
      const staff = await Staff.findOne({ _id: staff_id, company_id })
      const hourlyRate = staff.hourly_salary
      const rateInMinutes = (hourlyRate/60).toFixed(2)
      if (!hourlyRate) {
        throw new ErrorHandler(400, 'Hourly rate not defined')
      }
      const amount = (rateInMinutes * duration).toFixed(2)
      const date = new Date().toISOString().slice(0, 10)
      const latefine = await Latefine.findOne({ date, staff_id })
      if (latefine){
        throw new ErrorHandler(400, 'Latefine exists for today')
      } 
      const currentStatement = await SalaryStatement.findOne({ staff_id, date: { "$gte": startMonth, "$lte": endMonth}})
      if(!currentStatement){
        throw new ErrorHandler(400, "No statement found to add Latefine")
      }
      const newlLateFine = await Latefine.create({ staff_id, date, duration, amount  })
      if (!newlLateFine) {
        throw new ErrorHandler(400, 'Something went wrong adding latefine')
      }
      // Updating statement
      currentStatement.amount = (parseInt(currentStatement.amount) - parseInt(amount)).toFixed(2)
      const updateStatement = await currentStatement.save()
      res.json({
        status: 'ok',
        message: 'Latefine added successfully'
      })

    } catch (err) {
      next(err)
    }
});



module.exports = router