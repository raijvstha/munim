const express = require('express')
const router = express.Router()
const bodyParser = require('body-parser')

const auth = require('../middleware/auth')

const { runValidation } = require('../validators')
const { addAttendanceValidators, staffAttendanceCountValidators, allStaffAttendanceCountValidators } = require('../validators/attendance')
const { staffPermission, companyPermission } = require('../middleware/permission')
const { checkDate, checkStatement } = require('../middleware/check')
const { ErrorHandler } = require('../helpers/error')
const Attendance = require('../models/Attendance')
const Staff = require('../models/Staff')
const moment = require('moment');
const SalaryStatement = require('../models/SalaryStatement')
/**
 * @swagger
 *
 * /v1/attendance/add:
 *   post:
 *     description: Add Attendance of staff
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         description: Detail object of staff attendence to be added
 *         required: true
 *         schema:
 *            properties:
 *              staff_id:
 *                type: number
 *                example: "5f7c1e456a5d5041d77acf11"
 *                description: Change above example staff id with existing staff id 
 *              company_id:
 *                type: number
 *                example: "5f7b22a94a0e8633e4349f47"
 *                description: Change above example company id with existing company id
 *              attendance_type:
 *                type: string
 *                example: "present"
 *                enum:
 *                  - present
 *                  - halfday
 *                  - paidholiday 
 *                  - absent 
 *                  - hourly
 *     responses:
 *       201:
 *         description: Attendance done successfully
 *       400:
 *         description: Staff attendance was already done for today
 */

router.post('/add', [bodyParser.json(),  addAttendanceValidators, runValidation, staffPermission, checkDate, checkStatement], async (req, res, next) => {
  const { staff_id, attendance_type, hours } = req.body
  let present = false, halfday = false, paidholiday = false, absent = false
  const startMonth = new moment().startOf('month').format('YYYY-MM-DD[T00:00:00.000Z]')
  const endMonth = new moment().endOf("month").format('YYYY-MM-DD[T00:00:00.000Z]')
  try {
      if(attendance_type === "present"){
        present = true
      }
      if(attendance_type === 'halfday'){
        halfday = true
      }
      if(attendance_type === 'paidholiday'){
        paidholiday = true
      }
      if(attendance_type === 'absent') {
        absent = true
      }
      if(attendance_type === 'hourly') {
        present = true
        if(!hours){
          throw new ErrorHandler(400, 'Total working hours is required for this staff')
        }
      }
      const currentStatement = await SalaryStatement.findOne({ staff_id, date: { "$gte": startMonth, "$lte": endMonth}})
      if(!currentStatement){
        throw new ErrorHandler(400, "No statement found to add attendance data")
      }
      const attendentDate = new Date().toISOString().slice(0, 10)

      const checkAttendance = await Attendance.findOne({ staff_id, date: attendentDate })
      if(checkAttendance !== null){
        throw new ErrorHandler(400, 'Staff attendance was already done for today')
      }
      const attendanceResult = await Attendance.create({ staff_id, present, absent,  halfday, paidholiday, hours, date: attendentDate })
      if (!attendanceResult) {
          throw new ErrorHandler(400, 'Something went wrong on attending staff')
      }

      //Updating statement according to attendance
      const staff = await Staff.findOne({ _id : staff_id })
      const emplyment_type = staff.employment_type
      const perdaySalary = parseInt(staff.perday_salary)
      const halfdaySalary = (perdaySalary/2).toFixed(2)

      if(emplyment_type === 'weekly' || emplyment_type === 'daily' || emplyment_type === 'monthly' && attendance_type === 'absent'){
        currentStatement.amount = (parseInt(currentStatement.amount) - perdaySalary).toFixed(2)
      }

      if(emplyment_type === 'weekly' || emplyment_type === 'daily' || emplyment_type === 'monthly' && attendance_type === 'halfday'){
        currentStatement.amount = (parseInt(currentStatement.amount) - halfdaySalary).toFixed(2)
      }

      if(emplyment_type === 'hourly' && attendance_type === 'hourly'){
        currentStatement.amount = (parseInt(currentStatement.amount) + parseInt(staff.hourly_salary*hours)).toFixed(2)
      }

      const updateStatement = await currentStatement.save()

      res.status(201).json({
          status: 'ok',
          message: 'Attendance done successfully'
      })
  } catch (err) {
      next(err)
  }
});


/**
 * @swagger
 *
 * /v1/attendance/staff:
 *   post:
 *     description: Single Staff attendance details
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         description: Staff ID  Company ID and Date
 *         required: true
 *         schema:
 *            properties:
 *              staff_id:
 *                type: number
 *                example: "5f7c1e456a5d5041d77acf11"
 *                description: Change above example staff id with existing staff id 
 *              company_id:
 *                type: number
 *                example: "5f7b22a94a0e8633e4349f47"
 *                description: Change above example company id with existing company id
 *              date:
 *                type: date
 *                example: "2020-05-01"
 *                description: Date must be on YYYY-MM-DD format
 *              
 *     responses:
 *       200:
 *         status: ok,
 *         attendance: Object of attendance
 */

router.post('/staff', [bodyParser.json(), staffAttendanceCountValidators, runValidation, staffPermission], async (req, res, next) => {
  const { staff_id, date } = req.body
  try { 
    const startMonth = moment(date).format('YYYY-MM-DD[T00:00:00.000Z]');
    const endMonth = new moment(date).endOf("month").format('YYYY-MM-DD[T00:00:00.000Z]')
    const present = await Attendance.find({ date: { "$gte": startMonth, "$lte": endMonth}, staff_id, present: true }).countDocuments()
    const absent = await Attendance.find({ date: { "$gte": startMonth, "$lte": endMonth}, staff_id, absent: true }).countDocuments()
    const halfday = await Attendance.find({ date: { "$gte": startMonth, "$lte": endMonth}, staff_id, halfday: true }).countDocuments()
    const paidholiday = await Attendance.find({ date: { "$gte": startMonth, "$lte": endMonth}, staff_id, paidholiday: true }).countDocuments()
    const attendance = {
      present,
      absent,
      halfday,
      paidholiday,
    }
    res.json({
      status: 'ok',
      attendance
    })

  } catch (err) {
    next(err)
  }
  
});


/**
 * @swagger
 *
 * /v1/attendance/all:
 *   post:
 *     description: All Staff attendance details
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         description: Company ID and Date
 *         required: true
 *         schema:
 *            properties:
 *              user_id:
 *                type: number
 *                example: "5f7c1e456a5d5041d77acf11"
 *                description: Change above example user id with existing user id 
 *              company_id:
 *                type: number
 *                example: "5f7b22a94a0e8633e4349f47"
 *                description: Change above example company id with existing company id
 *              date:
 *                type: date
 *                example: "2020-05-01"
 *                description: Date must be on YYYY-MM-DD format
 *              
 *     responses:
 *       200:
 *         status: ok,
 *         attendance: Object of attendance
 */
router.post('/all', [bodyParser.json(), allStaffAttendanceCountValidators, runValidation, companyPermission], async (req, res, next) => {
  const { company_id } = req.body
  let { date } = req.body
  try { 
    date = moment(date).format('YYYY-MM-DD[T00:00:00.000Z]');
    const staffs = await Staff.find({ company_id, status: true })
    let present = 0, absent = 0, halfday = 0, paidholiday = 0, presentStaffs = []
    if(staffs && staffs.length > 0){
      for (staff of staffs){
        const attendance = await Attendance.findOne({ staff_id: staff._id, date }).select("present absent halfday paidholiday")
        //console.log(attendance)
        if(attendance && attendance.present === true){
          present = present + 1
          presentStaffs.push(staff)
        }
        if(attendance && attendance.absent === true){
          absent = absent + 1
        }
        if(attendance && attendance.halfday === true){
          halfday = halfday + 1
        }
        if(attendance && attendance.paidholiday === true){
          paidholiday = paidholiday + 1
        }
      }
      // staffs.map( async (staff, index) =>{
      //  // console.log(staff._id)
      //   const attendance = await Attendance.findOne({ staff_id: staff._id, date }).select("present absent halfday paidholiday")
      //   //console.log(attendance)
      //   if(attendance && attendance.present === true){
      //     present = present + 1
      //     presentStaffs.push(staff)
      //   }
      //   if(attendance && attendance.absent === true){
      //     absent = absent + 1
      //   }
      //   if(attendance && attendance.halfday === true){
      //     halfday = halfday + 1
      //   }
      //   if(attendance && attendance.paidholiday === true){
      //     paidholiday = paidholiday + 1
      //   }
        
      //   //console.log(presentIds)
      // })
    }

    // setTimeout(()=>{ 
    //   res.json({
    //     status: 'ok',
    //     attendance: {
    //       present,
    //       absent,
    //       halfday,
    //       paidholiday,
    //       presentStaffs
    //     }
    //   })
    // }, 100)

    res.json({
      status: 'ok',
      attendance: {
        present,
        absent,
        halfday,
        paidholiday,
        presentStaffs
      }
    })
    
    
  } catch (err) {
    next(err)
  }
  
});




module.exports = router