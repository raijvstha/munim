const express = require('express')
const router = express.Router()
const bodyParser = require('body-parser')

const auth = require('../middleware/auth')

const { runValidation } = require('../validators')
const { addOvertimeValidators, deleteOvertimeValidators } = require('../validators/overtime')
const { staffPermission } = require('../middleware/permission')
const { ErrorHandler } = require('../helpers/error')
const Overtime = require('../models/Overtime')



/**
 * @swagger
 *
 * /v1/overtime/add:
 *   post:
 *     description: Add Overtime to staff
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         description: Detail object of Overtime to be added
 *         required: true
 *         schema:
 *            properties:
 *              staff_id:
 *                type: number
 *                example: "5f7c1e456a5d5041d77acf11"
 *                description: Change above example staff id with existing staff id 
 *              company_id:
 *                type: number
 *                example: "5f7b22a94a0e8633e4349f47"
 *                description: Change above example company id with existing company id
 *              hourly_rate: 
 *                type: number
 *                example: "10"           
 *              duration:
 *                type: number
 *                example: "160"
 *                description: Duration must be in minutes
 * 
 *     responses:
 *       201:
 *         description: Overtime added successfully
 *       400:
 *         description: Overtime exists for today
 */

router.post('/add', [bodyParser.json(), addOvertimeValidators, runValidation, staffPermission], async (req, res, next) => {
    const {staff_id, hourly_rate, duration } = req.body
    const rateInMinutes = (hourly_rate/60).toFixed(2)
    const amount = (rateInMinutes * duration).toFixed(2)
    const date = new Date().toISOString().slice(0, 10)
     
    try {
      const overtime = await Overtime.findOne({ date, staff_id })
      if (overtime){
        throw new ErrorHandler(400, 'Overtime exists for today')
      } 
      const newOvertime = await Overtime.create({ staff_id, date, hourly_rate, duration, amount  })
  
      if (!newOvertime) {
        throw new ErrorHandler(400, 'Something went wrong adding overtime')
      }

      res.status(201).json({
        status: 'ok',
        message: 'Overtime added successfully'
      })
    } catch (err) {
      next(err)
    }
});


/**
 * @swagger
 *
 * /v1/overtime/delete:
 *   post:
 *     description: Delete Overtime of staff
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         description: Detail object of Overtime to be deleted
 *         required: true
 *         schema:
 *            properties:
 *              overtime_id:
 *                type: number
 *                example: "5f9519dbdf04fd6bbd7af8da"
 *                description: Change above example overtime id with existing overtime id 
 *              staff_id:
 *                type: number
 *                example: "5f7c1e456a5d5041d77acf11"
 *                description: Change above example staff id with existing staff id 
 *              company_id:
 *                type: number
 *                example: "5f7b22a94a0e8633e4349f47"
 *                description: Change above example company id with existing company id
 * 
 *     responses:
 *       200:
 *         description: Overtime deleted successfully
 *       404:
 *         description: Overtime exists for today
 *       400:
 *         desctiption: Unable to deleted overtime 
 */

router.post('/delete', [bodyParser.json(), deleteOvertimeValidators, runValidation, staffPermission], async (req, res, next) => {
  const { overtime_id } = req.body
  try {
    const overtime = await Overtime.findOne({ _id: overtime_id })
    if (!overtime){
      throw new ErrorHandler(404, 'Overtime does not exists')
    } 
    const newOvertime = await Overtime.findOneAndRemove({ _id: overtime_id  })
    const deleteOvertime = await Overtime.findOne({ _id: newOvertime._id })
    
    if (!deleteOvertime) {
      res.json({
        status: 'ok',
        message: 'Overtime deleted Successfully'
      })
    }else{
      res.statusCode(400).json({
        message: 'Unable to deleted overtime'
      })
    }

  } catch (err) {
    next(err)
  }
});



module.exports = router