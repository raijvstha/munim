const express = require('express')
const router = express.Router()
const bodyParser = require('body-parser')
const moment = require('moment')
const auth = require('../middleware/auth')

const { runValidation } = require('../validators')
const { staffValidators, allStaffsValidators } = require('../validators/staff')
const { reportValidators } = require('../validators/statement')
const { staffPermission, companyPermission } = require('../middleware/permission')
const { ErrorHandler } = require('../helpers/error')
const SalaryStatement = require('../models/SalaryStatement')
const Staff = require('../models/Staff')
const Payment = require('../models/Payment')
const Attendance = require('../models/Attendance')
const Overtime = require('../models/Overtime')
const Latefine = require('../models/Latefine')



/**
 * @swagger
 *
 * /v1/statement/staff-amount:
 *   post:
 *     description: Staff amount status
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         description: Staff ID  Company ID and Date
 *         required: true
 *         schema:
 *            properties:
 *              staff_id:
 *                type: number
 *                example: "5f7c1e456a5d5041d77acf11"
 *                description: Change above example staff id with existing staff id 
 *              company_id:
 *                type: number
 *                example: "5f7b22a94a0e8633e4349f47"
 *                description: Change above example company id with existing company id
 *              
 *     responses:
 *       200:
 *         status: ok,
 *         attendance: Staff Current Amount
 */
router.post('/staff-amount', [bodyParser.json(), staffValidators, runValidation, staffPermission], async (req, res, next) => {
  const { staff_id } = req.body
  const startMonth = new moment().startOf('month').format('YYYY-MM-DD[T00:00:00.000Z]')
  const endMonth = new moment().endOf("month").format('YYYY-MM-DD[T00:00:00.000Z]')
  try {
    const currentStatement = await SalaryStatement.findOne({ staff_id, date: { "$gte": startMonth, "$lte": endMonth}})
    if(!currentStatement){
      throw new ErrorHandler(400, "No statement found")
    }
    res.json({
      status: 'ok',
      message: 'Staff Current Amount',
      amount: currentStatement.amount
    })
  } catch (err) {
    next(err)
  }
});

/**
 * @swagger
 *
 * /v1/statement/company-amount:
 *   post:
 *     description: Company due amount
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         description: Staff ID  Company ID and Date
 *         required: true
 *         schema:
 *            properties:
 *              user_id:
 *                type: number
 *                example: "5f7c1e456a5d5041d77acf11"
 *                description: Change above example user id with existing user id 
 *              company_id:
 *                type: number
 *                example: "5f7b22a94a0e8633e4349f47"
 *                description: Change above example company id with existing company id
 *              
 *     responses:
 *       200:
 *         status: ok,
 *         attendance: Company current due amount
 */
router.post('/company-amount', [bodyParser.json(), allStaffsValidators, runValidation, companyPermission], async (req, res, next) => {
  const { company_id } = req.body
  const startMonth = new moment().startOf('month').format('YYYY-MM-DD[T00:00:00.000Z]')
  const endMonth = new moment().endOf("month").format('YYYY-MM-DD[T00:00:00.000Z]')
  let amount = 0
  try {
    const staffs = await Staff.find({ company_id })
    if(staffs.length > 0){
      for (staff of staffs){
        let statement = await SalaryStatement.findOne({ staff_id: staff._id, date: { "$gte": startMonth, "$lte": endMonth}})
        amount = amount + statement.amount
      }
    }
    res.json({
      status: 'ok',
      message: 'Company current due amount',
      amount
    })
  } catch (err) {
    next(err)
  }
});


/**
 * @swagger
 *
 * /v1/statement/payment-report:
 *   post:
 *     description: Staff payment report of current and previous month
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         description: Staff ID  Company ID and Date
 *         required: true
 *         schema:
 *            properties:
 *              user_id:
 *                type: number
 *                example: "5f7c1e456a5d5041d77acf11"
 *                description: Change above example user id with existing user id 
 *              company_id:
 *                type: number
 *                example: "5f7b22a94a0e8633e4349f47"
 *                description: Change above example company id with existing company id
 *              date:
 *                type: date
 *                example: "2020-05-01"
 *                description: Date must be on YYYY-MM-DD format
 *              
 *     responses:
 *       200:
 *         status: ok,
 *         attendance: Staff previous and current month report
 */

router.post('/payment-report', [bodyParser.json(), reportValidators, runValidation, companyPermission], async (req, res, next) => {
  const { company_id, date } = req.body
  const currentMonthStart = new moment(date).startOf('month').format('YYYY-MM-DD[T00:00:00.000Z]')
  const currentMonthEnd = new moment(date).endOf("month").format('YYYY-MM-DD[T00:00:00.000Z]')
  const prevMonth = moment(date).subtract(1, 'month').format("YYYY-MM-DD")
  const prevMonthStart = new moment(prevMonth).startOf('month').format('YYYY-MM-DD[T00:00:00.000Z]')
  const prevMonthEnd = new moment(prevMonth).endOf("month").format('YYYY-MM-DD[T00:00:00.000Z]')

  const prevMonthDate = moment(prevMonth).format("MMMM")
  const currentMonthDate = moment(date).format("MMMM")

  let paymentReport = []
  try{
    if(moment(date).isSame(moment().format("YYYY-MM-DD"), "year") === false || moment(date).isAfter(moment().format("YYYY-MM-DD"),  "month") === true){
      throw new ErrorHandler(400, "Year and month must not be greater than current year, month")
    }
    const staffs = await Staff.find({ company_id })
    if(staffs && staffs.length > 0){
      for(staff of staffs){
        const currentMonthStatement = await SalaryStatement.findOne({ staff_id: staff._id, date: { "$gte": currentMonthStart, "$lte": currentMonthEnd}})
        const previousMonthStatement = await SalaryStatement.findOne({ staff_id: staff._id, date: { "$gte": prevMonthStart, "$lte": prevMonthEnd}})

        const currentMonthPayments = await Payment.find({ staff_id: staff._id, date: { "$gte": currentMonthStart, "$lte": currentMonthEnd}}).select("amount")
        const previousMonthPayments = await Payment.find({ staff_id: staff._id, date: { "$gte": prevMonthStart, "$lte": prevMonthEnd}}).select("amount")

        
        let previousPayment = 0
        let currentPayment = 0
        let salary = 0

        if(staff.employment_type === "monthly"){
          salary = staff.monthly_salary
        }
        if(staff.employment_type === "hourly"){
          salary = staff.hourly_salary
        }
        if(staff.employment_type === "weekly"){
          salary = staff.weekly_salary
        }
        if(staff.employment_type === "daily"){
          salary = staff.perday_salary
        }

        if(currentMonthPayments && currentMonthPayments.length > 0){
          for (currentMonthPayment of currentMonthPayments){
            currentPayment = currentPayment + currentMonthPayment.amount
          }
        }
        if(previousMonthPayments && previousMonthPayments.length > 0){
          for (previousMonthPayment of previousMonthPayments){
            previousPayment = previousPayment + previousMonthPayment.amount
          }
        }

        const staffDetails = {
          name: staff.full_name,
          employment_type: staff.employment_type,
          prevMonth: prevMonthDate,
          currentMonth: currentMonthDate,
          current_month_payment: currentPayment,
          current_month_balance: currentMonthStatement !== null ? currentMonthStatement.amount : 0,
          previous_month_payment: previousPayment,
          previous_month_balance: previousMonthStatement !== null ? previousMonthStatement.amount : 0,
          salary
        }
        
        paymentReport.push(staffDetails)
      }
    }
    res.json({
      status: 'ok',
      message: 'Staff previous and current month report',
      paymentReport
    })
  }catch(err){
    next(err)
  }
});



/**
 * @swagger
 *
 * /v1/statement/attendance-report:
 *   post:
 *     description: Staff attendance report
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         description: Staff ID  Company ID and Date
 *         required: true
 *         schema:
 *            properties:
 *              user_id:
 *                type: number
 *                example: "5f7c1e456a5d5041d77acf11"
 *                description: Change above example user id with existing user id 
 *              company_id:
 *                type: number
 *                example: "5f7b22a94a0e8633e4349f47"
 *                description: Change above example company id with existing company id
 *              date:
 *                type: date
 *                example: "2020-05-01"
 *                description: Date must be on YYYY-MM-DD format
 *              
 *     responses:
 *       200:
 *         status: ok,
 *         attendance: Staff attendance repor
 */
router.post('/attendance-report', [bodyParser.json(), reportValidators, runValidation, companyPermission], async (req, res, next) => {
  const { company_id, date } = req.body
  const currentMonthStart = new moment(date).startOf('month').format('YYYY-MM-DD[T00:00:00.000Z]')
  const currentMonthEnd = new moment(date).endOf("month").format('YYYY-MM-DD[T00:00:00.000Z]')

  let attendanceReport = []
  try{
    if(moment(date).isSame(moment().format("YYYY-MM-DD"), "year") === false || moment(date).isAfter(moment().format("YYYY-MM-DD"),  "month") === true){
      throw new ErrorHandler(400, "Year and month must not be greater than current year, month")
    }
    const staffs = await Staff.find({ company_id })
    if(staffs && staffs.length > 0){
      for(staff of staffs){
        let overtimeDuration = 0
        let latefineDuration = 0
        const present = await Attendance.find({ date: { "$gte": currentMonthStart, "$lte": currentMonthEnd}, staff_id: staff._id, present: true }).countDocuments()
        const absent = await Attendance.find({ date: { "$gte": currentMonthStart, "$lte": currentMonthEnd}, staff_id: staff._id, absent: true }).countDocuments()
        const halfday = await Attendance.find({ date: { "$gte": currentMonthStart, "$lte": currentMonthEnd}, staff_id: staff._id, halfday: true }).countDocuments()
        const paidholiday = await Attendance.find({ date: { "$gte": currentMonthStart, "$lte": currentMonthEnd}, staff_id: staff._id, paidholiday: true }).countDocuments()

        const overtimes = await Overtime.find({ staff_id: staff._id, date: { "$gte": currentMonthStart, "$lte": currentMonthEnd}}).select("amount duration")
        const latefines = await Latefine.find({ staff_id: staff._id, date: { "$gte": currentMonthStart, "$lte": currentMonthEnd}}).select("amount duration")
        if(overtimes.length > 0){
          for(overtime of overtimes){
            overtimeDuration = overtimeDuration + overtime.duration
          }
        }
        if(latefines.length > 0){
          for(latefine of latefines){
            latefineDuration = latefineDuration + latefine.duration
          }
        } 
        let LFhours = 0
        let LFminutes = 0
        if(latefineDuration > 0){
          LFhours = Math.floor(latefineDuration / 60);          
          LFminutes = latefineDuration % 60;
        }
        let OThours = 0
        let OTminutes = 0
        if(overtimeDuration > 0){
          OThours = Math.floor(latefineDuration / 60);          
          OTminutes = latefineDuration % 60;
        }
         
        const staffDetails = {
          name: staff.full_name,
          present,
          absent,
          halfday,
          paidholiday,
          overtime: `${OThours}:${OTminutes}`,
          latefine: `${LFhours}:${LFminutes}`,
        }
        attendanceReport.push(staffDetails)
      }
    }
    res.json({
      status: 'ok',
      message: 'Staff attendance report',
      attendanceReport
    })
  }catch(err){
    next(err)
  }
  
});


module.exports = router