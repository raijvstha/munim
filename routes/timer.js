const express = require('express')
const router = express.Router()
const bodyParser = require('body-parser')
const moment = require('moment')
const auth = require('../middleware/auth')

const { runValidation } = require('../validators')
const { timerValidators } = require('../validators/timer')
const { ErrorHandler } = require('../helpers/error')
const { staffPermission } = require('../middleware/permission')
const { checkHourlyStaff } = require('../middleware/check')
const Timer = require('../models/Timer')



/**
 * @swagger
 *
 * /v1/timer/start:
 *   post:
 *     description: Start Timer
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         description: Staff Id and Company Id
 *         required: true
 *         schema:
 *            properties:
 *              staff_id:
 *                type: number
 *                example: "5f7c1e456a5d5041d77acf11"
 *                description: Change above example staff id with existing staff id 
 *              company_id:
 *                type: number
 *                example: "5f7b22a94a0e8633e4349f47"
 *                description: Change above example company id with existing company id
 *     responses:
 *       201:
 *         description: Timer started successfully 
 *       400:
 *         description: Timer already started for this staff
 */

router.post('/start', [bodyParser.json(), timerValidators, runValidation, staffPermission, checkHourlyStaff], async (req, res, next) => {
  const { staff_id } = req.body
  try{
    const timerStart = await Timer.findOne({ staff_id, date: moment().format("YYYY-MM-DD"), start_time: {"$lte": moment().format("hh:mm:ss")} })
    if(timerStart){
      throw new ErrorHandler(400, 'Timer already started for this staff')
    }
    const newTimerStart = await Timer.create({ 
      staff_id,
      start_time: moment().format("hh:mm:ss"),
      date: moment().format("YYYY-MM-DD"),
      status: "start"
    })
    if (!newTimerStart) {
      throw new ErrorHandler(400, 'Something went wrong starting timer')
    }
    res.status(201).json({
      status: 'ok',
      message: 'Timer started successfully',
      startTime: newTimerStart.start_time
    })
  }catch(err){
    next(err)
  }
});


/**
 * @swagger
 *
 * /v1/timer/end:
 *   post:
 *     description: Stop Timer
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         description: Staff Id and Company Id
 *         required: true
 *         schema:
 *            properties:
 *              staff_id:
 *                type: number
 *                example: "5f7c1e456a5d5041d77acf11"
 *                description: Change above example staff id with existing staff id 
 *              company_id:
 *                type: number
 *                example: "5f7b22a94a0e8633e4349f47"
 *                description: Change above example company id with existing company id
 * 
 *     responses:
 *       200:
 *         description: Timer stopped successfully
 *       404:
 *         description: No timer started
 */
router.post('/end', [bodyParser.json(), timerValidators, runValidation, staffPermission, checkHourlyStaff], async (req, res, next) => {
  const { staff_id } = req.body
  try{
    const startTimer = await Timer.findOne({ staff_id, date: moment().format("YYYY-MM-DD"), start_time: {"$lte": moment().format("hh:mm:ss")}, status: "start" })
    if(!startTimer){
      throw new ErrorHandler(404, 'No timer started')
    }
    const endTime = moment().format("hh:mm:ss")
    startTimer.end_time = endTime
    startTimer.status = "end"
    const endTimer  =  await startTimer.save()
    if(!endTimer) throw Error ('Unable to end timer')
     

    let start = endTimer.start_time
    let end = endTimer.end_time
    start = start.split(":");
    end = end.split(":");
    let startDate = new Date(0, 0, 0, start[0], start[1], start[2]);
    let endDate = new Date(0, 0, 0, end[0], end[1], end[2]);
    let diff = endDate.getTime() - startDate.getTime();
    let hours = Math.floor(diff / 1000 / 60 / 60);
    diff -= hours * 1000 * 60 * 60;
    let minutes = Math.floor(diff / 1000 / 60);
    
    diff -= minutes * 1000 * 60;
    let seconds = Math.floor(diff / 1000 );

    hours = hours < 10 ? `0${hours}`: hours
    minutes = minutes < 10 ? `0${minutes}`: minutes
    seconds = seconds < 10 ? `0${seconds}`: seconds

    res.json({
      status: 'ok',
      message: 'Timer stopped successfully',
      startTime: endTimer.start_time,
      endTime: endTimer.end_time,
      duration: `${hours}:${minutes}:${seconds}`
    }
      
    )
  }catch(err){
    next(err)
  }
});


// router.put('/reset', [bodyParser.json(), timerValidators, runValidation, staffPermission, checkHourlyStaff], async (req, res, next) => {
//   const { staff_id } = req.body
//   try{
//     const startTimer = await Timer.findOne({ staff_id, date: currentDate(), start_time: {"$lte": currentTime()}, status: "start" })
//     if(!startTimer){
//       throw new ErrorHandler(404, 'No timer started')
//     }
//     const deleteTimer = await Timer.findOneAndRemove({ _id: startTimer._id  })
//     res.json({
//       status: 'ok',
//       message: 'Timer reset successfully'
//     })
    
//   }catch(err){
//     next(err)
//   }
// });


module.exports = router