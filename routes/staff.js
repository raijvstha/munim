const express = require('express')
const router = express.Router()
const bodyParser = require('body-parser')
const moment = require('moment')

const auth = require('../middleware/auth')

const { runValidation } = require('../validators')
const { createStaffValidators, allStaffsValidators, hourlyRateValidators } = require('../validators/staff')
const { ErrorHandler } = require('../helpers/error')
const { companyPermission, staffPermission } = require('../middleware/permission')
const Staff = require('../models/Staff')
const SalaryStatement = require('../models/SalaryStatement')


/**
 * @swagger
 *
 * /v1/staff/register:
 *   post:
 *     description: Add staff to company
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         description:  Staff object to be added to company
 *         required: true
 *         schema:
 *            properties:
 *              full_name:
 *                type: string
 *                example: "testing staff"
 *              phone:
 *                type: number
 *                example: "9804050555"
 *              employment_type:
 *                type: string
 *                example: "monthly"
 *                enum:
 *                  - monthly
 *                  - weekly
 *                  - daily
 *                  - hourly
 *                  - work_basis
 *              salary:
 *                type: number
 *                example: "20000"
 *                description: Only required for monthly, weekly, daily, hourly employment type
 *              working_hours:
 *                type: number
 *                example: "8"
 *                description: Only required for monthly, weekly, daily, hourly employment type
 *              user_id:
 *                type: number
 *                example: "5f7b22a94a0e8633e4349f46"
 *                description: Change above example user id with existing user id 
 *              company_id:
 *                type: number
 *                example: "5f7b22a94a0e8633e4349f47"
 *                description: Change above example company id with existing company id
 *              staff_salary_cycle:
 *                type: number
 *                example: "25"
 *                description: Enter day on which salary should be paid
 *             
 * 
 *     responses:
 *       201:
 *         description: Staff registered successfully
 *       400:
 *         description: Staff exists with this phone number
 */

router.post('/register', [bodyParser.json(), createStaffValidators, runValidation, companyPermission, ], async (req, res, next) => {
  let { full_name, phone, employment_type, salary = "", working_hours = "", company_id, staff_salary_cycle, status } = req.body
  let monthly_salary = null, weekly_salary = null, perday_salary = null, hourly_salary = null, work_basis = null
  
  try {
    if(employment_type === 'monthly' && salary === "" ||  working_hours === ""){
      throw new ErrorHandler(422, `${salary === ""? 'Salary' :''}${working_hours === "" && salary === ""  ? ',' :''}${working_hours === "" ? 'Working hours' :''} ${working_hours === "" && salary === "" ? "are" : "is"} required for ${employment_type} employment type`)
    }else if(employment_type === 'weekly' && salary === "" ||  working_hours === ""){
      throw new ErrorHandler(422, `Salary is required for ${employment_type} employment type`)
    }else if(employment_type === 'daily' && salary === "" ||  working_hours === ""){
      throw new ErrorHandler(422, `Salary is required for ${employment_type} employment type`)
    }else if(employment_type === 'hourly' && salary === "" ||  working_hours === ""){
      throw new ErrorHandler(422, `Salary is required for ${employment_type} employment type`)
    }
    if(staff_salary_cycle >= 32 || staff_salary_cycle <= 1){
      throw new ErrorHandler(422, "Staff salary cycle must be between 1 to 32")
    }else {
      var currentTime = new Date()
      /* Get current month */
      var month = currentTime.getMonth() + 1
      if(month === 12){
        /* Get next month */
        month = 1
      }else{
        /* Get next month */
        month = month + 1
      }
      
      var year = currentTime.getFullYear()
      staff_salary_cycle = `${year}-${month}-${staff_salary_cycle}`
    }
    if(employment_type === 'monthly'){
      monthly_salary = salary
      perday_salary = ((monthly_salary/30)).toFixed(2)
      hourly_salary = ((monthly_salary/30)/working_hours).toFixed(2)
    }
    if(employment_type === 'weekly'){
      weekly_salary = salary
      perday_salary = (salary/6).toFixed(2)
      hourly_salary = (perday_salary/working_hours).toFixed(2)
    }
    if(employment_type === 'daily'){
      perday_salary = salary
      hourly_salary = (perday_salary/working_hours).toFixed(2)
    }
    if(employment_type === 'hourly'){
      hourly_salary = salary
    }
    if(employment_type === 'work_basis'){
      salary = null
      working_hours = null
    }
    const staff = await Staff.findOne({ phone })
    if(staff){
      throw new ErrorHandler(400, 'Staff exists with this phone number')
    }
    const newStaff = await Staff.create({ 
      company_id, 
      full_name, 
      phone, 
      employment_type, 
      monthly_salary,
      perday_salary,
      hourly_salary,
      weekly_salary,
      work_basis,
      working_hours,
      staff_salary_cycle,
      status  
    })
    if (!newStaff) {
      throw new ErrorHandler(400, 'Something went wrong adding staff')
    }
    let salaryAmount = 0
    const startMonth = new moment().startOf('month').format('YYYY-MM-DD[T00:00:00.000Z]')
    
    if(employment_type === 'monthly'){
      salaryAmount = monthly_salary
    }
    if(employment_type === 'weekly'){
      salaryAmount = weekly_salary*4
    }
    if(employment_type === 'daily'){
      salaryAmount = perday_salary*30
    }
    if(employment_type === 'hourly' || employment_type === 'work_basis' ){
      salaryAmount = 0
    }

    const statement = await SalaryStatement.create({ 
      staff_id: newStaff._id,
      date: startMonth,
      amount: salaryAmount
    })

    if (!statement) {
      throw new ErrorHandler(400, 'Something went wrong adding salary statement')
    }
    res.status(201).json({
      status: 'ok',
      message: 'Staff registered successfully'
    })

  } catch (err) {
    next(err)
  }
});


/**
 * @swagger
 *
 * /v1/staff/all:
 *   post:
 *     description: List all staff of company
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         description:  User Id and Company Id of which staff should be listed
 *         required: true
 *         schema:
 *            properties:
 *              user_id:
 *                type: number
 *                example: "5f7b22a94a0e8633e4349f46"
 *                description: Change above example user id with existing user id 
 *              company_id:
 *                type: number
 *                example: "5f7b22a94a0e8633e4349f47"
 *                description: Change above example company id with existing company id
 * 
 *     responses:
 *       200:
 *         description: Staffs listed Successfully
 *       404:
 *         description: No staffs found
 */

router.post('/all', [bodyParser.json(),  allStaffsValidators, runValidation, companyPermission], async (req, res, next) => {
  const { company_id } = req.body
  try {
    const staffs = await Staff.find({ company_id, status:"true" }).select("_id company_id full_name phone employment_type working_hours hourly_salary staff_salary_cycle status")
    if (!staffs) {
      throw new ErrorHandler(404, 'No staffs found')
    }
    res.json({
      status: 'ok',
      message: 'Staffs listed Successfully',
      staffs
    })

  } catch (err) {
    next(err)
  }
});



/**
 * @swagger
 *
 * /v1/staff/hourly-rate:
 *   post:
 *     description: Get hourly rate of staff from staff ID
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         description:  Staff Id and Company Id of which staff hourly rate should be listed
 *         required: true
 *         schema:
 *            properties:
 *              staff_id:
 *                type: number
 *                example: "5f7c1e456a5d5041d77acf11"
 *                description: Change above example staff id with existing staff id 
 *              company_id:
 *                type: number
 *                example: "5f7b22a94a0e8633e4349f47"
 *                description: Change above example company id with existing company id
 * 
 *     responses:
 *       200:
 *         description: Array of staff data
 *       404:
 *         description: No staffs found
 */

router.post('/hourly-rate', [bodyParser.json(),  hourlyRateValidators, runValidation, staffPermission], async (req, res, next) => {
  const { staff_id } = req.body
  try {
    //const staffs = await Staff.findOne({ staff_id }).select("_id company_id full_name phone employment_type working_hours staff_salary_cycle status")
    const staff = await Staff.findOne({ _id: staff_id }).select("hourly_salary")
    if (!staff) {
      throw new ErrorHandler(404, 'No staffs found')
    }
    res.json({
      status: 'ok',
      staff
    })

  } catch (err) {
    next(err)
  }
});


/**
 * @swagger
 *
 * /v1/staff/id:
 *   post:
 *     description: Get staff detalils by staff ID
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         description:  Staff Id and Company Id of which staff hourly rate should be listed
 *         required: true
 *         schema:
 *            properties:
 *              staff_id:
 *                type: number
 *                example: "5f7c1e456a5d5041d77acf11"
 *                description: Change above example staff id with existing staff id 
 *              company_id:
 *                type: number
 *                example: "5f7b22a94a0e8633e4349f47"
 *                description: Change above example company id with existing company id
 * 
 *     responses:
 *       200:
 *         description: Array of staff data
 *       404:
 *         description: No staffs found
 */

router.post('/id', [bodyParser.json(),  hourlyRateValidators, runValidation, staffPermission], async (req, res, next) => {
  const { staff_id } = req.body
  try {
    //const staffs = await Staff.findOne({ staff_id }).select("_id company_id full_name phone employment_type working_hours staff_salary_cycle status")
    const staff = await Staff.findOne({ _id: staff_id })
    if (!staff) {
      throw new ErrorHandler(404, 'No staffs found')
    }
    res.json({
      status: 'ok',
      staff
    })

  } catch (err) {
    next(err)
  }
});


/**
 * @swagger
 *
 * /v1/staff/delete-staff:
 *   post:
 *     description: Delete staff by staff ID
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         description:  Staff Id and Company Id of which staff should be deleted
 *         required: true
 *         schema:
 *            properties:
 *              staff_id:
 *                type: number
 *                example: "5f7c1e456a5d5041d77acf11"
 *                description: Change above example staff id with existing staff id 
 *              company_id:
 *                type: number
 *                example: "5f7b22a94a0e8633e4349f47"
 *                description: Change above example company id with existing company id
 * 
 *     responses:
 *       200:
 *         description: Staffs deleted Successfully
 *       400:
 *         description: Unable to deleted staff
 */

router.post('/delete-staff', [bodyParser.json(),  hourlyRateValidators, runValidation, staffPermission], async (req, res, next) => {
  const { staff_id } = req.body
  try {
    const staff = await Staff.findOneAndRemove({ _id: staff_id })
    const delStaff = await Staff.findOne({ _id: staff_id })
    if (!delStaff) {
      res.json({
        status: 'ok',
        message: 'Staffs deleted Successfully'
      })
    }else{
      res.statusCode(400).json({
        message: 'Unable to deleted staff'
      })
    }
    
    //console.log(staff_id)

  } catch (err) {
    next(err)
  }
});



module.exports = router