const express = require('express')
const router = express.Router()
const bodyParser = require('body-parser')

const auth = require('../middleware/auth')

const { runValidation } = require('../validators')
const { addDecuctionValidators, editDeductionValidators } = require('../validators/deduction')
const { staffValidators } = require('../validators/staff')
const { staffPermission } = require('../middleware/permission')
const { ErrorHandler } = require('../helpers/error')
const Deduction = require('../models/Deduction')



/**
 * @swagger
 *
 * /v1/deduction/add:
 *   post:
 *     description: Add Deductions to staff
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         description: Detail object of Deductions to be added
 *         required: true
 *         schema:
 *            properties:
 *              staff_id:
 *                type: number
 *                example: "5f7c1e456a5d5041d77acf11"
 *                description: Change above example staff id with existing staff id 
 *              company_id:
 *                type: number
 *                example: "5f7b22a94a0e8633e4349f47"
 *                description: Change above example company id with existing company id
 *              type:
 *                type: string
 *                example: "PF"
 *                enum:
 *                  - ESI
 *                  - PF
 *                  - Other
 *              amount: 
 *                type: number
 *                example: "500"
 *              date:
 *                type: date
 *                example: "2020-03-30"
 *                description: Date must be on YYYY-MM-DD format
 *              description:
 *                type: string
 *                example: "Deduction added"
 * 
 *     responses:
 *       201:
 *         description: Deduction added successfully
 *       400:
 *         description: SSI, PF or Other Deduction type already added to this staff
 */

router.post('/add', [bodyParser.json(), addDecuctionValidators, runValidation, staffPermission], async (req, res, next) => {
    const { staff_id, amount, date, description, type } = req.body
    try {

    const deduction = await Deduction.findOne({ staff_id, type })
    if (deduction){
      throw new ErrorHandler(400, `${type} Deduction type already added to this staff`)
    } 
    const newDeduction = await Deduction.create({ staff_id, date, description, amount, type  })

    if (!newDeduction) {
      throw new ErrorHandler(400, 'Something went wrong adding deduction')
    }

    res.status(201).json({
      status: 'ok',
      message: 'Deduction added successfully'
    })

    } catch (err) {
      next(err)
    }
});


/**
 * @swagger
 *
 * /v1/deduction/edit:
 *   put:
 *     description: Edit Deduction
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         description: Deduction Id
 *         required: true
 *         schema:
 *            properties:
 *              deduction_id:
 *                type: number
 *                example: "5f7b22a94a0e8633e4349f47"
 *                description: Change above example deduction id with existing deduction id
 *              staff_id:
 *                type: number
 *                example: "5f7c1e456a5d5041d77acf11"
 *                description: Change above example staff id with existing staff id 
 *              company_id:
 *                type: number
 *                example: "5f7b22a94a0e8633e4349f47"
 *                description: Change above example company id with existing company id
 * 
 *     responses:
 *       201:
 *         description: Deduction updated successfully
 *       400:
 *         description: No any deduction is added
 */

router.put('/edit', [bodyParser.json(), editDeductionValidators, runValidation, staffPermission], async (req, res, next) => {
  const {deduction_id, staff_id, amount, date, description, type} = req.body 
  try {
    const deduction = await Deduction.findOne({ _id: deduction_id, staff_id })
    if(!deduction){
      throw new ErrorHandler(404, 'No any deduction is added')
    }
    deduction.amount = amount ? amount : deduction.amount
    deduction.description = description ? description : deduction.description
    deduction.date = date ? date : deduction.date
    deduction.type = type ? type : deduction.type
    const updatedDeduction  =  await deduction.save()
    if(!updatedDeduction) throw Error ('Something went wrong updating Deduction')
    res.json({
      status: 'ok',
      message: 'Deduction updated successfully',
      updatedDeduction
    })
  } catch (err) {
      next(err)
  }
});



/**
 * @swagger
 *
 * /v1/deduction/list:
 *   post:
 *     description: List of deductions of staff
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         description:  Staff Id
 *         required: true
 *         schema:
 *            properties:
 *              company_id:
 *                type: number
 *                example: "5f7b22a94a0e8633e4349f47"
 *                description: Change above example company id with existing company id
 *              staff_id:
 *                type: number
 *                example: "5f7c1e456a5d5041d77acf11"
 *                description: Change above example staff id with existing staff id 
 * 
 *     responses:
 *       200:
 *         description: Lists of staff deductions
 *       404:
 *         description: No any deductions added
 */

router.post('/list', [bodyParser.json(), staffValidators, runValidation, staffPermission], async (req, res, next) => {
  const { staff_id } = req.body
  try {
    const deductions = await Deduction.find({ staff_id })
    //console.log(typeof deductions)
    if(deductions.length === 0){
      throw new ErrorHandler(404, 'No any deductions added')
    }
    res.json({
      status: 'ok',
      message: 'Lists of staff deductions',
      deductions
    })
  } catch (err) {
    next(err)
  }
});


module.exports = router