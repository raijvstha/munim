const { check } = require('express-validator')

exports.addDecuctionValidators = [
  check('staff_id')
    .not().isEmpty().withMessage('Staff Id is required'),
  check('company_id')
    .not().isEmpty().withMessage('Company Id is required'),
  check('amount')
    .not()
    .isEmpty()
    .withMessage('Amount is required'),
  check('date')
    .optional()
    .isDate()
    .isISO8601('yyyy-mm-dd')
    .withMessage('Enter valid date yyyy-mm-dd'),
  check('description')
    .not()
    .isEmpty()
    .withMessage('Description is required'),
  check('type')
    .not().isEmpty().withMessage('Deduction Type is required')
    .isIn(['ESI', 'PF', 'Other']).withMessage('Deduction Type must be ESI, PF or Other'),

];




exports.editDeductionValidators = [
  check('deduction_id')
    .not().isEmpty().withMessage('Deduction Id is required'),
  check('staff_id')
    .not().isEmpty().withMessage('Staff Id is required'),
  check('company_id')
    .not().isEmpty().withMessage('Company Id is required'),
  check('amount')
    .optional()
    .not()
    .isEmpty()
    .withMessage('Amount is required'),
  check('date')
    .optional()
    .isDate()
    .isISO8601('yyyy-mm-dd')
    .withMessage('Enter valid date yyyy-mm-dd'),
  check('description')
    .optional()
    .not()
    .isEmpty()
    .withMessage('Description is required'),
  check('type')
    .optional()
    .not().isEmpty().withMessage('Deduction Type is required')
    .isIn(['ESI', 'PF', 'Other']).withMessage('Deduction Type must be ESI, PF or Other'),
];