const { check } = require('express-validator')

exports.addAttendanceValidators = [
  check('staff_id')
    .not()
    .isEmpty()
    .withMessage('Staff Id is required'),
  check('company_id')
    .not()
    .isEmpty()
    .withMessage('Company Id is required'),
  check('attendance_type')
    .isIn(['present', 'absent', 'halfday', 'paidholiday', 'hourly'])
    .withMessage('Attendance type must be present, absent, halfday, paidholiday', 'hourly'),
];

exports.staffAttendanceCountValidators = [
  check('staff_id')
    .not()
    .isEmpty()
    .withMessage('Staff Id is required'),
  check('company_id')
    .not()
    .isEmpty()
    .withMessage('Company Id is required'),
  check('date')
    .not()
    .isEmpty()
    .withMessage('Date is required')
    .isDate()
    .isISO8601('yyyy-mm-dd')
    .withMessage('Enter valid date yyyy-mm-dd'),
];

exports.attendanceCountValidators = [
  check('staff_id')
    .not()
    .isEmpty()
    .withMessage('Staff Id is required'),
  check('company_id')
    .not()
    .isEmpty()
    .withMessage('Company Id is required'),
  check('date')
    .not()
    .isEmpty()
    .withMessage('Date is required')
    .isDate()
    .isISO8601('yyyy-mm-dd')
    .withMessage('Enter valid date yyyy-mm-dd'),
];


exports.allStaffAttendanceCountValidators = [
  check('user_id')
    .not()
    .isEmpty()
    .withMessage('User Id is required'),
  check('company_id')
    .not()
    .isEmpty()
    .withMessage('Company Id is required'),
  check('date')
    .not()
    .isEmpty()
    .withMessage('Date is required')
    .isDate()
    .isISO8601('yyyy-mm-dd')
    .withMessage('Enter valid date yyyy-mm-dd'),
];

