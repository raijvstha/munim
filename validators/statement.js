const { check } = require('express-validator')

exports.reportValidators = [
  check('user_id')
    .not().isEmpty().withMessage('User Id is required'),
  check('company_id')
    .not().isEmpty().withMessage('Company Id is required'),
  check('date')
    .isDate()
    .isISO8601('yyyy-mm-dd')
    .withMessage('Enter valid date'),
];
