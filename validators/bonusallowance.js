const { check } = require('express-validator')

exports.addBonusAllowanceValidators = [
  check('staff_id')
    .not()
    .isEmpty()
    .withMessage('Staff Id is required'),
  check('company_id')
    .not()
    .isEmpty()
    .withMessage('Company Id is required'),
  check('amount')
    .not()
    .isEmpty()
    .withMessage('Amount is required'),
  check('date')
    .not().isEmpty().withMessage('Date is required')
    .isDate().isISO8601('yyyy-mm-dd').withMessage('Enter valid date'),
  check('description')
    .not()
    .isEmpty()
    .withMessage('Description is required'),
  check('type')
    .not().isEmpty().withMessage('Type is required value must be "allowance" or "bonus"')
    .isIn(['allowance', 'bonus']).withMessage('Type must be allowance or bonus'),
];

exports.allBonusAllowanceValidators = [
  check('staff_id')
    .not()
    .isEmpty()
    .withMessage('Staff Id is required'),
  check('company_id')
    .not()
    .isEmpty()
    .withMessage('Company Id is required'),
];



