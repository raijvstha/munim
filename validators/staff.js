const { check } = require('express-validator')

exports.createStaffValidators = [
  check('company_id')
    .not().isEmpty().withMessage('Company Id is required'),
  check('full_name')
    .not().isEmpty().withMessage('Name is required'),
  check('phone')
    .not().isEmpty().withMessage('Phone is required')
    .isNumeric().withMessage('Phone must be number'),
  check('employment_type')
    .not().isEmpty().withMessage('Employment Type is required')
    .isIn(['monthly', 'weekly', 'daily', 'hourly', 'work_basis']).withMessage('Employment Type must be monthly, weekly, daily, hourly or work_basis'),
  check('salary')
    .optional()
    .not().isEmpty().withMessage('Salary is required')
    .isNumeric().withMessage('Salary must be number'),
  check('working_hours')
    .optional()
    .not().isEmpty().withMessage('Working hours is required')
    .isNumeric().withMessage('Working hours must be number'),
  check('staff_salary_cycle')
    .not().isEmpty().withMessage('Salary Cycle is required')
    .isNumeric().withMessage('Salary Cycle must be number'),
];

exports.allStaffsValidators = [
  check('user_id')
    .not().isEmpty().withMessage('User Id is required'),
  check('company_id')
    .not().isEmpty().withMessage('Company Id is required')
];

exports.hourlyRateValidators = [
  check('staff_id')
    .not().isEmpty().withMessage('Staff Id is required'),
  check('company_id')
    .not().isEmpty().withMessage('Company Id is required')
];

exports.staffValidators = [
  check('staff_id')
    .not().isEmpty().withMessage('Staff Id is required'),
  check('company_id')
    .not().isEmpty().withMessage('Company Id is required')
];
