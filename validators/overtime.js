const { check } = require('express-validator')

exports.addOvertimeValidators = [
  check('staff_id')
    .not().isEmpty().withMessage('Staff Id is required'),
  check('company_id')
    .not().isEmpty().withMessage('Company Id is required'),
  check('hourly_rate')
    .not().isEmpty().withMessage('Hourly rate is required')
    .isNumeric().withMessage('Hourly rate must be number'),
  check('duration')
    .not().isEmpty().withMessage('Duration is required')
    .isNumeric().withMessage('Duration must be numbers as minute')
    
];

exports.deleteOvertimeValidators = [
  check('staff_id')
    .not().isEmpty().withMessage('Staff Id is required'),
  check('company_id')
    .not().isEmpty().withMessage('Company Id is required'),
  check('overtime_id')
    .not().isEmpty().withMessage('Overtime Id is required')
];