const { check } = require('express-validator')

exports.addLatefineValidators = [
  check('staff_id')
    .not().isEmpty().withMessage('Staff Id is required'),
  check('company_id')
    .not().isEmpty().withMessage('Company Id is required'),
  check('duration')
    .not().isEmpty().withMessage('Duration is required')
    .isNumeric().withMessage('Duration must be numbers as minute'),
];