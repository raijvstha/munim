const { check } = require('express-validator')

exports.timerValidators = [
  check('staff_id')
    .not().isEmpty().withMessage('Staff Id is required'),
  check('company_id')
    .not().isEmpty().withMessage('Company Id is required')
];