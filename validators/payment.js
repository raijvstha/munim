const { check } = require('express-validator')

exports.calculateSalaryValidators = [
  check('staff_id')
    .not().isEmpty().withMessage('Staff Id is required'),
  check('company_id')
    .not().isEmpty().withMessage('Company Id is required')
];
exports.paySalaryValidators = [
  check('staff_id')
    .not().isEmpty().withMessage('Staff Id is required'),
  check('company_id')
    .not().isEmpty().withMessage('Company Id is required'),
  check('amount')
    .not().isEmpty().withMessage('Amount is required'),
  check('date')
    .isDate().isISO8601('yyyy-mm-dd').withMessage('Enter valid date'),
  check('description')
    .not().isEmpty().withMessage('Description is required')
];

exports.salaryListsValidators = [
  check('staff_id')
    .not().isEmpty().withMessage('Staff Id is required'),
  check('company_id')
    .not().isEmpty().withMessage('Company Id is required'),
  check('date')
    .isDate().isISO8601('yyyy-mm-dd').withMessage('Enter valid date')
]