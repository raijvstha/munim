const { validationResult } = require('express-validator')
const { ErrorHandler } = require('../helpers/error')

exports.runValidation = (req, res, next) => {
    const errors = validationResult(req)
    if(!errors.isEmpty()){
        throw new ErrorHandler( 422, errors.array()[0].msg )
    }
    next();
} 