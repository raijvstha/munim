const { check } = require('express-validator')

exports.userListValidators = [
  check('user_id')
    .not().isEmpty().withMessage('User Id is required')
];

exports.createCompanyValidators = [
  check('user_id')
    .not().isEmpty().withMessage('User Id is required'),
  check('company_name')
    .not().isEmpty().withMessage('Company Name is required')
];

exports.editCompanyValidators = [
  check('user_id')
    .not().isEmpty().withMessage('User Id is required'),
  check('company_id')
    .not().isEmpty().withMessage('Company Id is required'),
  check('company_name')
    .optional()
    .not()
    .isEmpty()
    .withMessage('Company Name is required')
];