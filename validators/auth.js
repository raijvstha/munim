const { check } = require('express-validator')

exports.userSignupValidators = [
  check('phone')
    .not().isEmpty().withMessage('Phone is required')
    .isNumeric().withMessage('Phone must be number'),
  check('company_name')
    .not().isEmpty().withMessage('Company Name is required'),
  check('full_name')
    .not().isEmpty().withMessage('Full Name is required'),
  
];



exports.userLoginValidators = [
  check('phone')
    .not().isEmpty().withMessage('Phone is required')
    .isNumeric().withMessage('Phone must be number'),
  // check('email')
  //   .not().isEmpty().withMessage('Email address is required')
  //   .isEmail().withMessage('Must be a valid email address'),
  // check('password')
  //   .not().isEmpty().withMessage('Password is required')
];


exports.searchbyphoneValidators = [
  check('phone')
    .not().isEmpty().withMessage('Phone is required')
    .isNumeric().withMessage('Phone must be number'),
];

exports.userUpdateValidators = [
  check('user_id')
    .not().isEmpty().withMessage('User Id is required'),
  check('full_name')
    .optional()
    .not().isEmpty().withMessage('Full Name is required')
];