const { check } = require('express-validator')

exports.addLoanValidators = [
  check('staff_id')
    .not()
    .isEmpty()
    .withMessage('Staff Id is required'),
  check('company_id')
    .not()
    .isEmpty()
    .withMessage('Company Id is required'),
  check('amount')
    .not()
    .isEmpty()
    .withMessage('Amount is required'),
  check('date')
    .isDate()
    .isISO8601('yyyy-mm-dd')
    .withMessage('Enter valid date'),
  check('description')
    .not()
    .isEmpty()
    .withMessage('Description is required'),
];


exports.editLoanValidators = [
  check('staff_id')
    .not()
    .isEmpty()
    .withMessage('Staff Id is required'),
  check('loan_id')
    .not()
    .isEmpty()
    .withMessage('Loan Id is required'),
  check('company_id')
    .not()
    .isEmpty()
    .withMessage('Company Id is required'),
  check('amount')
    .optional()
    .not()
    .isEmpty()
    .withMessage('Amount is required'),
  check('date')
    .optional()
    .isDate()
    .isISO8601('yyyy-mm-dd')
    .withMessage('Enter valid date yyyy-mm-dd'),
  check('description')
    .optional()
    .not()
    .isEmpty()
    .withMessage('Description is required')
];

exports.payBackLoanValidators = [
  check('staff_id')
    .not()
    .isEmpty()
    .withMessage('Staff Id is required'),
  check('loan_id')
    .not()
    .isEmpty()
    .withMessage('Loan Id is required'),
  check('company_id')
    .not()
    .isEmpty()
    .withMessage('Company Id is required'),
  check('amount')
    .not()
    .isEmpty()
    .withMessage('Amount is required'),
  check('date')
    .isDate()
    .isISO8601('yyyy-mm-dd')
    .withMessage('Enter valid date'),
  check('description')
    .not()
    .isEmpty()
    .withMessage('Description is required'),
];


exports.getstaffLoansValidators = [
  check('staff_id')
    .not()
    .isEmpty()
    .withMessage('Staff Id is required'),
  check('company_id')
    .not()
    .isEmpty()
    .withMessage('Company Id is required')
];

exports.deletestaffLoansValidators = [
  check('loan_id')
    .not()
    .isEmpty()
    .withMessage('Loan Id is required'),
  check('staff_id')
    .not()
    .isEmpty()
    .withMessage('Staff Id is required'),
  check('company_id')
    .not()
    .isEmpty()
    .withMessage('Company Id is required')
];

