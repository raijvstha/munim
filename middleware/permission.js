const { ErrorHandler } = require('../helpers/error')
const Staff = require('../models/Staff')
const Company = require('../models/Company')

const staffPermission = async (req, res, next) => {
    const { staff_id, company_id } = req.body
    try{
        const staff = await Staff.findOne({company_id, _id: staff_id})
        if(!staff) throw new ErrorHandler(404, "Staff does not exist")
        next()
    }catch(err){
        next(err)
    }
}

const companyPermission = async (req, res, next) => {
    const { user_id, company_id } = req.body
    try{
        const companyDetails = await Company.findOne({ _id: company_id }).populate("user_id", "full_name")
        if(!companyDetails){
            throw new ErrorHandler(404, "Company doesnot exists")
        } else{
            if(companyDetails.user_id._id != user_id){
                throw new ErrorHandler(401, "Unauthorize Access")
            }
        }
        next()
    }catch(err){
        next(err)
    }
}

module.exports = {
    staffPermission, 
    companyPermission
}