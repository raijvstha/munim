const expressJwt = require('express-jwt');

module.exports = expressJwt({ 
  secret: process.env.JWT_SECRET,  
  algorithms: ['HS256']
})

// module.exports = ( req, res, next) => {
//       var token = req.headers['x-access-token'] || req.headers['authorization']
//       if (token.startsWith('Bearer ')) {
//           // Remove Bearer from string
//           token = token.slice(7, token.length);
//       }
  
//       // Check for token
//       if (!token)
//           return res.status(401).json({ msg: 'No token, authorizaton denied' });
  
//       try {
//           // Verify token
//           const decoded = jwt.verify(token, config.JWT_SECRET);
//           // Add user from payload
//           req.user = decoded;
//           next();
//       } catch (err) {
//           res.status(400).json({ msg: 'Token is not valid' });
//       }
//   }