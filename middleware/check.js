const { ErrorHandler } = require('../helpers/error')
const Staff = require('../models/Staff')
const moment = require('moment')
const SalaryStatement = require('../models/SalaryStatement')

const checkHourlyStaff = async (req, res, next) => {
    const { staff_id } = req.body
    try{
        const staff = await Staff.findOne({ _id: staff_id, employment_type: "hourly" })
        if(!staff){
          throw new ErrorHandler(404, "This staff is not hourly staff")
        } 
        next()
    }catch(err){
        next(err)
    }
}

const checkDate = async (req, res, next) => {
  let { date } = req.body
  date = date ? date : moment().format("YYYY-MM-DD")
  try{
    const sameYear = moment(date).isSame(moment().format("YYYY-MM-DD"), 'year')
    const sameMonth = moment(date).isSame(moment().format("YYYY-MM-DD"), 'month')
    if(!sameYear || !sameMonth){
      throw new ErrorHandler(400, "Please enter current month")
    }  
      next()
  }catch(err){
      next(err)
  }
}

const checkStatement = async (req, res, next) => {
  let { staff_id, date } = req.body
  date = date ? date : moment().format("YYYY-MM-DD")
  const startMonth = new moment(date).startOf('month').format('YYYY-MM-DD[T00:00:00.000Z]')
  const endMonth = new moment(date).endOf("month").format('YYYY-MM-DD[T00:00:00.000Z]')
  try{
    const currentStatement = await SalaryStatement.findOne({ staff_id, date: { "$gte": startMonth, "$lte": endMonth}})
    if (!currentStatement) {
      const prevMonth = moment(date).subtract(1, 'months')
      const prevStartMonth = new moment(prevMonth).startOf('month').format('YYYY-MM-DD[T00:00:00.000Z]')
      const prevEndMonth = new moment(prevMonth).endOf("month").format('YYYY-MM-DD[T00:00:00.000Z]')
      const prevMonthStatement = await SalaryStatement.findOne({ staff_id, date: { "$gte": prevStartMonth, "$lte": prevEndMonth}})
      if(!prevMonthStatement){
        throw new ErrorHandler(400, "Prevous statement not found to create new statement")
      }
      const newStatement = await SalaryStatement.create({ 
        staff_id: staff_id,
        date: startMonth,
        amount: prevMonthStatement.amount
      })
    }
    next()
  }catch(err){
      next(err)
  }
}

module.exports = {
  checkHourlyStaff,
  checkDate,
  checkStatement
}