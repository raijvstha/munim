const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema({
    staff_id: {
        // type: mongoose.Schema.Types.ObjectId, 
        // ref: 'Staff',
        type: String,
        required: true
    },
    loan_id: {
      type: String,
      required: true
    },
    amount: {
      type: Number,
      required: true
    },
    date: {
      type: Date,
      required: true
    }
},{ timestamps: true })

module.exports  = mongoose.model('Loan', UserSchema)