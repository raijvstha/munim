const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema({
    staff_id: {
        // type: mongoose.Schema.Types.ObjectId, 
        // ref: 'Staff',
        type: String,
        required: true
    },
    // company_id: {
    //     type: mongoose.Schema.Types.ObjectId, 
    //     ref: 'Company',
    //     required: true
    // },
    amount: {
        type: Number,
        required: true
    },
    paid_amount: {
        type: Number,
        required: false
    },
    payback_amount: {
        type: Number,
        required: false
    },
    issue_date: {
        type: Date,
    },
    payback_date: {
        type: Date,
    },
    description: {
        type: String,
        required: true
    },
    payback_description: {
        type: String,
        required: false
    },
    status: {
        type: String,
        required: true, 
        enum: ["unpaid", "paid"]
    },
    pay_back_every_month: {
        type: String,
        required: false, 
        trim: true,
        enum: ["true", "false"]
    },
},{ timestamps: true })

module.exports  = mongoose.model('Loan', UserSchema)