const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema({
    username: {
        type: String,
        trim: true,
        required: true,
        maxlength: 32,
        unique: true,
        index: true,
        lowercase: true
    },
    full_name: {
        type: String,
        trim: true,
        required: true
    }, 
    email: {
        type: String,
        unique: true,
        trim: true,
        required: false,
        lowercase: true
    },
    password: {
        type: String,
        required: true
    },
    role: {
        type: String,
        required: true, 
        enum: ["admin"]
    }
},{timestamps: true})

module.exports  = mongoose.model('AdminUser', UserSchema)