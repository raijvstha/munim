const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema({
  staff_id: {
    // type: mongoose.Schema.Types.ObjectId, 
    // ref: 'Staff',
    type: String,
    required: true
  },
  date: {
    type: Date,
    require: true
  },
  present: {
    type: Boolean,
    default: false
  },
  absent: {
    type: Boolean,
    default: false
  },
  halfday: {
    type: Boolean,
    default: false
  },
  paidholiday: {
    type: Boolean,
    default: false
  },
  hours: {
    type: Number
  }
  
},{timestamps: true})

module.exports  = mongoose.model('Attendance', UserSchema)