const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema({
    // username: {
    //     type: String,
    //     trim: true,
    //     required: true,
    //     maxlength: 32,
    //     unique: true,
    //     index: true,
    //     lowercase: true
    // },
    phone: {
        type: Number,
        required: true,
        unique: true,
        trim: true,
    }, 
    full_name: {
        type: String,
        trim: true,
        required: true
    }, 
    email: {
        type: String,
        unique: true,
        trim: true,
        required: false,
        lowercase: true
    },
    
    // password: {
    //     type: String,
    //     required: true
    // },
    role: {
        type: String,
        required: true, 
        enum: ["owner", "staff"]
    }
},{timestamps: true})

module.exports  = mongoose.model('User', UserSchema)