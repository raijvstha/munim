const mongoose = require('mongoose')

const CompanySchema = new mongoose.Schema({
    company_name: {
        type: String,
        trim: true,
        required: true,
        unique: true
    },
    user_id: { 
        required: true,
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'User' 
    }
},{timestamps: true})

module.exports  = mongoose.model('Company', CompanySchema)