const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema({
    company_id: {
        // type: mongoose.Schema.Types.ObjectId, 
        // ref: 'Company',
        type: String,
        required: true
    },
    full_name: {
        type: String,
        trim: true,
        required: true
    }, 
    phone: {
        type: Number,
        required: true,
        unique: true
    },
    employment_type: {
        type: String,
        required: true, 
        enum: ["monthly", "weekly", "daily", "hourly", "work_basis"]
    },
    monthly_salary: {
        type: Number,
        required: false,
    },
    weekly_salary: {
        type: Number,
        required: false,
    },
    perday_salary: {
        type: Number,
        required: false,
    },
    hourly_salary: {
        type: Number,
        required: false,
    },
    work_basis: {
        type: Number,
        required: false,
    },
    working_hours:{
        type: Number,
        required: false,
    },
    staff_salary_cycle:{
        type: Date,
        required: true,
    },
    status: {
        type: Boolean,
        default: true
    }
},{timestamps: true})

module.exports  = mongoose.model('Staff', UserSchema)

