const mongoose = require('mongoose')

const TimerSchema = new mongoose.Schema({
    staff_id: {
        type: String,
        required: true
    },
    start_time: {
        type: String,
        required: false
    }, 
    end_time: {
        type: String,
        required: false
    },
    date:{
        type: String,
        required: true,
    },
    status:{
      type: String,
      required: true, 
      trim: true,
      enum: ["start", "end"]
    }
},{timestamps: true})

module.exports  = mongoose.model('Timer', TimerSchema)

