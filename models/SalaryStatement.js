const mongoose = require('mongoose')

const SalaryStatementSchema = new mongoose.Schema({
    staff_id: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        required: true
    },
    amount: {
        type: Number,
        required: true
    }
},{timestamps: true})

module.exports  = mongoose.model('SalaryStatement', SalaryStatementSchema)