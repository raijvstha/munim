const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema({
    staff_id: {
        // type: mongoose.Schema.Types.ObjectId, 
        // ref: 'Staff',
        type: String,
        required: true
    },
    date: {
        type: Date,
        required: true
    },
    hourly_rate: {
        type: Number,
        required: true
    },
    duration: {
        type: Number,
        required: true
    },
    amount: {
        type: Number,
        required: true
    },
},{timestamps: true})

module.exports  = mongoose.model('Overtime', UserSchema)