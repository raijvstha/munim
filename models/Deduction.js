const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema({
    staff_id: {
        // type: mongoose.Schema.Types.ObjectId, 
        // ref: 'Staff',
        type: String,
        required: true
    },
    date: {
        type: Date,
        required: true
    },
    amount: {
        type: Number,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    type: {
        type: String,
        required: true, 
        enum: ["ESI", "PF", "Other"]
    },
},{timestamps: true})

module.exports  = mongoose.model('Deduction', UserSchema)