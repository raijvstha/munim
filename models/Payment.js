const mongoose = require('mongoose')

const PaymentSchema = new mongoose.Schema({
    staff_id: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        required: true
    },
    amount: {
        type: Number,
        required: true
    },
    description: {
        type: String,
        required: true
    },
},{timestamps: true})

module.exports  = mongoose.model('Payment', PaymentSchema)