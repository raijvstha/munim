const express = require('express')
const cors = require('cors')
const mongoose = require('mongoose')
const swaggerJSDoc = require('swagger-jsdoc')
const swaggerUi = require('swagger-ui-express')
const cookieParser = require('cookie-parser')
const dotenv = require('dotenv')
dotenv.config()
const { handleError } = require('./helpers/error')

const authRoutes = require('./routes/auth')
const companyRoutes = require('./routes/company')
const staffRoutes = require('./routes/staff')
const loanRoutes = require('./routes/loan')
const attendanceRoutes = require('./routes/attendance')
const bonusallowanceRoutes = require('./routes/bonusallowance')
const overtimeRoutes = require('./routes/overtime')
const latefineRoutes = require('./routes/latefine')
const deductionRoutes = require('./routes/deduction')
const paymentRoutes = require('./routes/payment')
const timerRoutes = require('./routes/timer')
const statementRoutes = require('./routes/statement')


const app = express()


// swagger setup
const swaggerOptions = {
  swaggerDefinition: {
    info: {
      title: 'Munim API',  
      version: '1.0.0',
    },
  },
  servers: ["http://localhost:4000"],
  apis: ['./routes/*.js'],
}

const swaggerDocs = swaggerJSDoc(swaggerOptions)
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs));

// Middleware
app.use(cookieParser())
app.use(cors())

// Routes
app.use('/v1', authRoutes);
app.use('/v1/company', companyRoutes);
app.use('/v1/staff', staffRoutes);
app.use('/v1/loan', loanRoutes);
app.use('/v1/attendance', attendanceRoutes);
app.use('/v1/bonus-allowance', bonusallowanceRoutes);
app.use('/v1/overtime', overtimeRoutes);
app.use('/v1/latefine', latefineRoutes);
app.use('/v1/deduction', deductionRoutes);
app.use('/v1/payment', paymentRoutes);
app.use('/v1/timer', timerRoutes);
app.use('/v1/statement', statementRoutes);




app.use((error, req, res, next) => {
  handleError(error, res)
})


// Mongodb connect with mongoose
mongoose.connect(
    process.env.MONGO_URL,
    { 
        useNewUrlParser: true, 
        useUnifiedTopology: true,
        useCreateIndex: true, 
        useFindAndModify: false
    }
).then(() => {
  console.log('MongoDB Connected ...')
  // Starting server
  const PORT = process.env.PORT || 4000;
  app.listen(PORT, () => console.log(`🚀 Server ready at  http://localhost:${PORT}`));

}).catch(err => console.log(err))
